---
title: My contributions to open source - May 2022
date: 31 May, 2022
author: eamanu
tags: new-blog, Debian, python, open-source
---
# My contribution to Open Source - May 2022

## Debian
This is the list of the contributions:

* pylev: New upstream release 1.4.0. Close: [#1000440](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1000440). Uploaded by [Jeroen Ploemen](https://tracker.debian.org/news/1323303/accepted-pylev-140-1-source-into-unstable/).
* python-blosc: New upstream release 1.10.6. Uploaded by [Graham Inggs](https://tracker.debian.org/news/1325601/accepted-python-blosc-1106ds1-1-source-into-unstable/)
* python-marshmallow-sqlalchemy: New upstream reelease. Uploaded by [Jeroen Ploemen](https://tracker.debian.org/news/1324557/accepted-python-marshmallow-sqlalchemy-0280-1-source-into-unstable/).
* python-cassandra-driver: Working in the new upstream release 3.25.0. Fixing [#1000612](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1000612). Waiting for sponsorship.
* python-flask-jwt-extended: New upstream release 4.4.0. Uploaded to unstable https://tracker.debian.org/news/1326013/accepted-python-flask-jwt-extended-440-1-source-all-into-unstable/
* markdown-it-py: New upstream release 2.1.0. Uploaded to unstable https://tracker.debian.org/news/1326421/accepted-markdown-it-py-210-1-source-all-into-unstable/.
* fsspec: New upstream release: 2022.5.0. Uploaded by [Jeroen Ploemen](https://tracker.debian.org/news/1328732/accepted-fsspec-202250-1-source-into-unstable/)
* jcc: New upstream release 3.12. Uploaded https://tracker.debian.org/news/1328313/accepted-jcc-312-1-source-amd64-into-unstable/
* ftputil: New upstream release 5.0.4. Uploaded by [Jeroen Ploemen](https://tracker.debian.org/news/1328736/accepted-python-ftputil-504-1-source-into-unstable/)

# Other projects
* python-driver (DataStax): Fix Typos detected by Litnian during the packaging in Debian. https://github.com/datastax/python-driver/pull/1126
