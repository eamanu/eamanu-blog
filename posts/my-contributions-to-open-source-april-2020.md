---
author: eamanu
blogspot: true
date: May 02, 2020
tags: Old-Blog
title: "My contributions to Open Source \u2013 April 2020"

---
# My contributions to Open Source – April 2020

April was a complicated month (adding the quarantine issue), so I cannot dedicate the time that I wish.

According to the COVID-19 issue I join to the debian-med team to help a little with packages related to medicine. I am working on idseq-dag in a test fail <https://github.com/chanzuckerberg/idseq-dag/issues/295>.

Also, I try the first contact to upsteam to try to persuade to use a license <https://github.com/kentnf/VirusDetect/issues/1>.

Typo on gdal: https://github.com/OSGeo/gdal/pull/2432

The last version of pys3landsat is the v0.2.4: <https://github.com/eamanu/pyamazonlandsat/releases> fixing minor bugs.

On Debian, I package python3-rnc2rng <https://tracker.debian.org/pkg/rnc2rng>, on upstream I propose add support for py37 and py38 <https://github.com/djc/rnc2rng/pull/23>. Also change the Licensse <https://github.com/djc/rnc2rng/issues/21>. Then, make minor change on README <https://github.com/djc/rnc2rng/pull/26>

Also, I am woking on python3-hypothesis new upstream release.