---
title: My contributions to Free Software - May 2023
date: 31 May, 2023
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - May 2023

## Debian
This is the list of the contributions:
* myst-parser: new upstrean version 1.0.0. Package go to experimental. Waiting for sponsorship.
* shellingham: new upstream version 1.5.1. Package go to experimental. Uploaded by [Santiago Ruano Rincón](https://tracker.debian.org/news/1431609/accepted-shellingham-151-1exp1-source-into-experimental/)
* scikit-build: New upstream version 0.17.5. Package go to experimental. Waiting for sponsorship.
* monty: New upstream release version 2023.4.10. Package go to experimental. Waiting for sponsorship.
* python-scp: New upstream release version 0.14.5. Package go to experimental. Uploaded by [Santiago Ruano Rincón](https://tracker.debian.org/news/1432647/accepted-python-scp-0145-1exp1-source-into-experimental/)
* mdit-py-plugins: New upstream release version 0.3.5. package go to experimental. Uploaded by [Santiago Ruano Rincón](https://tracker.debian.org/news/1431537/accepted-mdit-py-plugins-035-1exp1-source-into-experimental/)

# Other projects
* PyPi Warehouse: I resumed a PR that I started for EuroPython2020, yes to long time, but thanks to Warehose mantainers to ping
me about this isuse. The issue is [#1981](https://github.com/pypi/warehouse/issues/1981) and the fix is is [this](https://github.com/pypi/warehouse/pull/8323). Now,
when you have several login failed, you will receive a message with a little bit more information :-).
