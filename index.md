# My personal notes :)

Hello World! Find more about me here: [about](./about#about-me)

I'm not an expert, don't try to learn from me :-).

Here you can find about Debian, Python and perhaps other things.

You can se my most recents entries here:

```{postlist} 10
---
author: me
tags: new-blog
---
```


Here is a list of some of my entries in my old blog:

```{postlist} 10
---
author: me
sort:
tags: Old-Blog
---
```

```{toctree}
:hidden:
:glob:

about.md
```
