---
title: My contributions to Free Software - December 2022
date: 31 December, 2022
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - December 2022

## Debian
This is the list of the contributions:

This are during the [Debian Python Sprint](https://wiki.debian.org/Sprints/2022/PythonTeam): 
 * Poetry: New upstream release poetry\_1.2.2+dfsg-1. Uploaded to [unstable](https://tracker.debian.org/news/1393375/accepted-poetry-122dfsg-1-source-into-unstable/)
 * fix packages running tests with "python3 setup.py tests" during the Debian Python Sprint 2022:
   - blist: stop run python3 setup.py tests
   - flask-basicauth: stop run python3 setup.py tests; Stop using pypy module in debhelper sequences; Bump Standards-Version to 4.6.1.1;
   - pep8: stop run python3 setup.py tests; fix autopkgtests; Clarify descriptions binary package (fixing #985883)
   - python-django-ical: stop run python3 setup.py tests;
   - python-freesasa: stop run python3 setup.py tests;
   - phonenumbers: stop run python3 setup.py tests;
 * scalene: New upstream release 1.5.16. Several improves. Closes [#1025237](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1025237), [#1023633](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1023633). Uploaded by [Stefano Rivera](https://tracker.debian.org/news/1402547/accepted-scalene-1516-1-source-into-unstable/)
 * mdit-py-plugins: New upstream release 0.3.3. Uploaded to [unstable](https://tracker.debian.org/news/1401132/accepted-mdit-py-plugins-033-1-source-into-unstable/).
 * palettable: Update package. Uploaded to [unstable](https://tracker.debian.org/news/1186041/palettable-330-2-migrated-to-testing/).
 * python-blosc: New upstream release 1.11.1. Uploaded to [unstable](https://tracker.debian.org/news/1403469/accepted-python-blosc-1111ds1-1-source-into-unstable/).
 * crashtest: New upstream release 0.4.1. Uploaded to [unstable](https://tracker.debian.org/news/1405013/accepted-crashtest-041-1-source-into-unstable/).
 * python-xlib: New upstream release 0.33. Waiting for sponsorship.
 


# Other projects
Sadly no :(.
