---
author: eamanu
blogspot: true
date: Apr 27, 2017
tags: Old-Blog
title: Emacs como IDE de python

---
# Emacs como IDE de python

Si, emacs como IDE de python. Emacs es una poderosa herramienta (y dificil) pero una vez que le agarras la mano, quieres usar emacs con todo. Este es un ejemplo. Voy a mostrar como integrar python a emacs.

Python shell






Emacs trae por defecto python. Para abrir una consola de python simplemente debemos escribir el siguiente comando:

<div class="wp-synhighlighter-outer" id="wpshdo_8"><div class="wp-synhighlighter-expanded" id="wpshdt_8"><table border="0" width="100%"><tr><td align="left" width="80%"><a name="#codesyntax_8"></a>[Source code](#codesyntax_8 "Click to show/hide code block")</td><td align="right">[![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/code.png)](#codesyntax_8 "Show code only") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/printer.png)](#codesyntax_8 "Print code") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/info.gif)](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/About.html "Show plugin information")</td></tr></table>

</div><div class="wp-synhighlighter-inner" id="wpshdi_8" style="display: block;">```
<pre class="bash" style="font-family:monospace;">M-x run-python
```

</div></div>Elpy

-

Elpy es una herramientas que nos brinda varias funcionalidades como por ejemplo el autocompletado, leer documentación, ejecución de tests, etc.

Les dejo [documentación](https://media.readthedocs.org/pdf/elpy/latest/elpy.pdf) y el repositorio en [github](https://github.com/jorgenschaefer/elpy).

Para instalar el complemento, en primer lugar debemos agregar las siguientes líneas en ~/.emacs. (Si sos nuevo como yo, seguramente no vas a encontrar este archivo, simplemente tenes que crear un archivo llamado .emacs en ~/)

<div class="wp-synhighlighter-outer" id="wpshdo_9"><div class="wp-synhighlighter-expanded" id="wpshdt_9"><table border="0" width="100%"><tr><td align="left" width="80%"><a name="#codesyntax_9"></a>[Source code](#codesyntax_9 "Click to show/hide code block")</td><td align="right">[![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/code.png)](#codesyntax_9 "Show code only") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/printer.png)](#codesyntax_9 "Print code") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/info.gif)](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/About.html "Show plugin information")</td></tr></table>

</div><div class="wp-synhighlighter-inner" id="wpshdi_9" style="display: block;">```
<pre class="lisp" style="font-family:monospace;"><span class="br0">(</span>require 'package<span class="br0">)</span>
<span class="br0">(</span>add-to-<span class="kw1">list</span> 'package-archives
             '<span class="br0">(</span><span class="st0">"elpy"</span> <span class="sy0">.</span> <span class="st0">"http://jorgenschaefer.github.io/packages/"</span><span class="br0">)</span><span class="br0">)</span>
```

</div></div>Y luego actualizamos la lista de paquetes haciendo

<div class="wp-synhighlighter-outer" id="wpshdo_10"><div class="wp-synhighlighter-expanded" id="wpshdt_10"><table border="0" width="100%"><tr><td align="left" width="80%"><a name="#codesyntax_10"></a>[Source code](#codesyntax_10 "Click to show/hide code block")</td><td align="right">[![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/code.png)](#codesyntax_10 "Show code only") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/printer.png)](#codesyntax_10 "Print code") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/info.gif)](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/About.html "Show plugin information")</td></tr></table>

</div><div class="wp-synhighlighter-inner" id="wpshdi_10" style="display: block;">```
<pre class="bash" style="font-family:monospace;">M-x package-refresh-contents
```

</div></div>Por último instalamos el paquete haciendo:

<div class="wp-synhighlighter-outer" id="wpshdo_11"><div class="wp-synhighlighter-expanded" id="wpshdt_11"><table border="0" width="100%"><tr><td align="left" width="80%"><a name="#codesyntax_11"></a>[Source code](#codesyntax_11 "Click to show/hide code block")</td><td align="right">[![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/code.png)](#codesyntax_11 "Show code only") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/printer.png)](#codesyntax_11 "Print code") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/info.gif)](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/About.html "Show plugin information")</td></tr></table>

</div><div class="wp-synhighlighter-inner" id="wpshdi_11" style="display: block;">```
<pre class="bash" style="font-family:monospace;">M-x package-install RET elpy RET
```

</div></div>Y una vez que está instalado lo activamos haciendo:

<div class="wp-synhighlighter-outer" id="wpshdo_12"><div class="wp-synhighlighter-expanded" id="wpshdt_12"><table border="0" width="100%"><tr><td align="left" width="80%"><a name="#codesyntax_12"></a>[Source code](#codesyntax_12 "Click to show/hide code block")</td><td align="right">[![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/code.png)](#codesyntax_12 "Show code only") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/printer.png)](#codesyntax_12 "Print code") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/info.gif)](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/About.html "Show plugin information")</td></tr></table>

</div><div class="wp-synhighlighter-inner" id="wpshdi_12" style="display: block;">```
<pre class="bash" style="font-family:monospace;"><span class="br0">(</span>elpy-enable<span class="br0">)</span>
```

</div></div>También es necesario instalar algunas bibliotecas para que funcione correctamente:

elpy jedi flake8 importmagic autopep8 yapf epc (con un *pip install* lo haces)

Funcionamiento




--

Cuando ya tenga todo instalado, ya podes empezar a tirar tus lineas de código en emacs. Vas a ver que ya está funcionando el autocompletado:

[![](../uploads/2017/04/emacs@eamanu_001.png)](https://eamanu.com/blog/wp-content/uploads/2017/04/emacs@eamanu_001.png) [![](../uploads/2017/04/emacs@eamanu_002.png)](https://eamanu.com/blog/wp-content/uploads/2017/04/emacs@eamanu_002.png)

Con el comando

```
<pre lang="lisp">C-c C-d
```

podes ver la documentación.

[![](../uploads/2017/04/emacs@eamanu_003.png)](https://eamanu.com/blog/wp-content/uploads/2017/04/emacs@eamanu_003.png)

También tiene la capacidad de indicarte cuando estás rompiendo con algunos de las indicaciones del PEP8. Así que vas a poder tener tu código siguiente el *codestyle* recomendado.

[![](../uploads/2017/04/emacs@eamanu_004.png)](https://eamanu.com/blog/wp-content/uploads/2017/04/emacs@eamanu_004.png)

Y para enviar el código a la consola se debe ejecutar

```
<pre lang="lisp">C-c C-c
```

[![](../uploads/2017/04/emacs@eamanu_006.png)](https://eamanu.com/blog/wp-content/uploads/2017/04/emacs@eamanu_006.png)