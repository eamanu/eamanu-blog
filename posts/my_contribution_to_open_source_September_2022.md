---
title: My contributions to open source - September 2022
date: 30 September, 2022
author: eamanu
tags: new-blog, Debian, python, open-source
---
# My contribution to Open Source - September 2022

## Debian
This is the list of the contributions:
* crashtest: New upstream release 0.4.0. Uploaded to unstable by [Jeroen Ploemen](https://tracker.debian.org/news/1358852/accepted-crashtest-040-1-source-into-unstable/).
* fsspec: New upstream release 2022.8.2. Uploaded to unstable by [Jeroen Ploemen](https://tracker.debian.org/news/1360622/accepted-fsspec-202282-1-source-into-unstable/).
* shellingham: New upstream release 1.5.0. Uploaded to unstable by [Jeroen Ploemen](https://tracker.debian.org/news/1362045/accepted-shellingham-150-1-source-into-unstable/).
* python-marshmallow-sqlalchemy: new upstream release 0.28.1. Uploaded to unstable by [Jeroen Ploemen](https://tracker.debian.org/news/1324557/accepted-python-marshmallow-sqlalchemy-0280-1-source-into-unstable/).

# Other projects

* [Open issue](https://github.com/marshmallow-code/marshmallow-sqlalchemy/issues/462) in marshmallow\_sqlalchemy. DeprectationWarning in tests still happed

