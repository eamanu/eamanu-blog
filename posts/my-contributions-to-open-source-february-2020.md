---
author: eamanu
blogspot: true
date: Mar 17, 2020
tags: Old-Blog
title: "My contributions to Open Source \u2013 February 2020"

---
# My contributions to Open Source – February 2020

I’m now Debian Maintainer (<https://eamanu.com/blog/soy-debian-maintainer/>) 🙂 so, it’s time to spend more time on Debian. I was working on severels bugs de Debian and I receive uploads permisssions to flask-jwt-simple and flask-jwt-extended (<https://tracker.debian.org/pkg/flask-jwt-simple> and <https://tracker.debian.org/pkg/python-flask-jwt-extended>). Flask jwt simple was my first upload 🙂 (<https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=945767>). Also I was trying to clean my backlog tasks <https://qa.debian.org/developer.php?login=emmanuelarias30%40gmail.com>. I introduce `scalene` and `libcamera` to NEW. Also I was working on `coccinelle`.

I am the coordinate of FLISOL La Rioja 2020, so with help of my [University](https://www.unlar.edu.ar/) (Marcelo Martinez, Juan Pablo Millicay and Luis Bazan) and the Free and Open Source La Rioja Coomunnity we are organizing this edition of FLISOL. (Now in stand by for the COVID19) <https://flisol.info/FLISOL2020/Argentina/LaRioja>

![](../uploads/2020/03/WhatsApp-Image-2020-03-16-at-21.21.45.jpeg)Because of Flisol I was testing EventoL system so, I published some simples issues ([\#634](https://github.com/eventoL/eventoL/issues/634), [\#633](https://github.com/eventoL/eventoL/issues/633)) and a PR ([\#635](https://github.com/eventoL/eventoL/pull/635))

Also I still working on deprecate creation of asyncio object when the loop is not running for cpython (<https://github.com/python/cpython/pull/1819>) Disscussion here: <https://bugs.python.org/issue38599>