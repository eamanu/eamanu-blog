---
title: My contributions to Free Software - October 2024
date: 31 Oct, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - October 2024

## Debian
* rust-crc: New upstream release 3.2.1.
* rust-crc-catalog: New upstream release 2.4.0
* rsut-dolby-vision: New upstream release 3.3.1
* kiwisolver: New upstream release 1.4.7. Stop installing metadata in /usr/lib/.
  See [#1063463](https://bugs.debian.org/1063463)
* scalene: New upstream release 1.5.45.
* rust-fontdb: New upstream release 0.22.0.
* rust-svg-metadata: New usptream release 0.5.1.
* rust-unicode-bidi-mirroring: New upstream release 0.4.0.
* rust-unicode-ccc: New upstream release 0.4.0.
* contourpy: Fix FTBFS [#1081660](https://bugs.debian.org/1081660) to build
  contourpy with py3.13.
* pcapy: Fix FTBFS [#1081430](https://bugs.debian.org/1081430) to build pcapy with py3.13.
* scalene: scalene: Remove dependency on astunparse in version 1.5.45-2.
* pytkdocs: Remove dependency on astunparse in version 0.16.2-2.
* advocate: Add patch to fix SyntaxWarning [#1085321](https://bugs.debian.org/1085321). In version 1.0.0-6.1.
* apycula: Add patch to fix SyntaxWarning [#1085325](https://bugs.debian.org/1085325). In version 0.14+dfsg1-2.

### Sponsored Package
* rust-magnus: Introduce version 0.6.3 to Debian. Kudos to Ananthu C V
  <weepingclown@disroot.org>.
* rust-pnet-datalink: Introduce version 0.35.0 to Debian. Kudos to Ananthu C V
  <weepingclown@disroot.org>.
* rust-wezterm-dynamic: Introduce version 0.2.0 to Debian. Kudos to Ananthu C V
  <weepingclown@disroot.org>.
* rust-wezterm-input-types: Introduce version 0.1.0 to Debian. Kudos to Ananthu C V
  <weepingclown@disroot.org>.
* rust-finl-unicode: Introduce new version 1.2.0 to Debian. Kudos to Ananthu C V
  <weepingclown@disroot.org>.
* rust-memmem: Introduce new version 0.1.1 to Debian. Kudos to Ananthu C V
  <weepingclown@disroot.org>.
* rust-wezterm-blob-leases: Introduce new version 0.1.0 to Debian. Kudos to Ananthu C V
  <weepingclown@disroot.org>.
* python-pyaarlo: New upstream release 0.8.0.8. Kudos to Carles Pina i Estany
  <carles@pina.cat>.
* rust-k9: Introduce version 0.12.0 to Debian. Kudos to Ananthu C V
  <weepingclown@disroot.org>.
* rust-wezterm-bidi: New upstream release 0.2.3 to Debian. Kudos to Ananthu C V 
  <weepingclown@disroot.org>. 
* rust-wezterm-color-types: Introduce version 0.3.0 to Debian. Kudos to Ananthu
  C V  <weepingclown@disroot.org>.
* rust-vtparse: Introduce version 0.6.2 to Debian. Kudos to Ananthu
  C V  <weepingclown@disroot.org>.
* python3-pytest-vcr: Introduce new version 1.0.2 to Debian. Kudos to Ananthu
  C V  <weepingclown@disroot.org>.
* python-pyaarlo: New upstream release 0.8.0.11. Kudos to Carles Pina i Estany
  <carles@pina.cat>.
* rust-rustybuzz: Introduce version 0.17.0 to Debian. Kudos to NoisyCoil
  <noisycoil@tutanota.com>.
* rust-termwiz: Introduce version 0.22.0 to Debian. Kudos to Ananthu
  C V  <weepingclown@disroot.org>.
* rust-deadpool: Introduce version 0.12.1 to Debian. Kudos to Ananthu
  C V  <weepingclown@disroot.org>.


## Other projects
* impacket: Open issue related to FTBFS in Debian. See [#1824](https://github.com/fortra/impacket/issues/1824)
* apicula: Fix SyntaxWarning in upstream. See [PR#286](https://github.com/YosysHQ/apicula/pull/286).
