---
author: eamanu
blogspot: true
date: Dec 13, 2020
tags: Old-Blog
title: "My contributions to Open Source \u2013 November 2020"

---
# My contributions to Open Source – November 2020

Working on Debian Python Team on fixing FTBFS for python3.9-defaults. Also:

- python3-blosc\_1.9.2+ds1-1 new upstream release suporting py3.9 (uploaded by: Matthias Klose)
- python3-skbuild\_0.11.1 now in Debian (thanks Matthias Klose)
- Packaging s2-geometry, waiting for sponsorship https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=974967
- python3-molotov: new upstream release 2.0. Waitting for aiodogstatsd
- python3-aiodogstatsd: packaging and uploaded to unstable (Thanks Matthias Klose)
- Adopt and move to Debian Python Team of python-requests-toolbelt. Waiting sponsor https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=943419

Retake the work for package python-poetry on Debian https://lists.debian.org/debian-python/2020/12/msg00001.html I hope finish it before Debian freeze

Develop simple tool to know if whole dependency of a Python module are missing on Debian https://github.com/eamanu/pydebdep

This issue created on python-blosc upstream: https://github.com/Blosc/python-blosc/issues/235

Create a new project: https://github.com/eamanu/unlarnianos A forums for my University based on flaskbb project.

I participate on PyConAr2020 with the talk «Join to translate the official Python Documentation» https://eventos.python.org.ar/events/pyconar2020/activity/410/ &lt;3 \\o/ So, in this month I help to python-docs-es, making minor PRs and many translation reviews.(Very happy to be involved on this projects)

Also, we’re almost at the end of the course «Intermidiate Python» for UNLAR.