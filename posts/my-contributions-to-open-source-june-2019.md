---
author: eamanu
blogspot: true
date: Jul 11, 2019
tags: Old-Blog
title: My contributions to Open Source &#8211; June 2019

---
# My contributions to Open Source &#8211; June 2019

![](../uploads/2019/07/image.png)On CPython’s asyncio module for public API the loop parameters for that is deprecated on python 3.8 and planned to remove on 3.10. So, I work on [bpo-36373](https://github.com/python/cpython/pull/13950). I divide the work on different PR for each module: task.py, queue.py, stream.py, locks.py. For that, a warns message may be launch and should be inform on documentation. Also, the unit tests may wrapped with ``with self.assertWarns(DeprecationWarning`)`

I start packaging the library: Libcamera (http://libcamera.org). The salsa repository is: https://salsa.debian.org/eamanu-guest/libcamera and I create a Github Repository: http://github.com/eamanu/libcamera

Also, I start contribute to escrutinio-social, and I start with a PR adding Travis to the repository. Also, I was make some Code Review and help a little on README.md and INSTALLATION.md

I create a PR on cherrypy fixing the copyright year.

I create a PR to cxxopts library adding a propose of raise a exception. This repository is a library for manage argparse for C++. I create the PR: https://github.com/jarro2783/cxxopts/pull/186 to fix a segfaults situation (https://github.com/jarro2783/cxxopts/issues/185.)

Also, I fix a example on documentation of flask-pymongo.

Also I make some PR review:

![](../uploads/2019/07/image-1.png)Also I start the Open Data La Rioja (I will make a entry for that)