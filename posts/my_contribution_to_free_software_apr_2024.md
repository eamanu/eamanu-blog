---
title: My contributions to Free Software - April 2024
date: 30 April, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - April 2024

## Debian
* commonmark: Adopt [orphaned package](https://bugs.debian.org/1065151).
* cachy: Close a false positive RC bug [#](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1067321).
* cppy: Adopt [orphaned package](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065248).
* monty: New upstream release 2024.3.31.
* kiwisolver: Adopt [orphaned package](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065244).
* lazy-object-proxy: Adopt [orphaned package](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065238).
* apispec: New upstream release 6.6.0. Uploaded to unstable.
* bidict: New upstream release 0.23.1. Uploaded to unstable.
* kiwisolver: New upstream release 1.4.5. Uploaded to unstable.
* fsspec: New upstream release 2024.3.1. Uploaded to unstable.
* monty: New upstream release 2024.4.17. Uploaded to unstable.
* pygithub: New upstream release 2.3.0. Uploaded to unstable.
* i3pystatus: Fix RC bug [#1056410](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1056410) to get support to Python3.12.
* python-cassandra-driver: New upstream release 3.29.1. Uploaded to unstable.
* scalene: New upstream release 1.5.39. Uploaded to unstable.
* myst-parser: New upstream release 3.0.0. Uploaded to unstable.
* cppy: Fix autopkgtest.
* scikit-build-core: New upstream release 0.9.2. Uploaded to unstable.
* scikit-build: Fix RC bug [1069805](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1069805).
* scalene: New upstream release 1.5.40. Uploaded to unstable.
* rnc2rng: New upstream release _2.7.0. Uploaded to unstable.

### Sponsored Package

* python-shtab: New upstream release 1.7.1. Kudos to Felix Moessbauer <felix.moessbauer@siemens.com>.
* python-asv-runner: Introduce to Debian version 0.2.1. Kudos Yogeswaran Umasankar <kd8mbd@gmail.com>.

## Other projects
* poetry: Make a fail in command install if cannot be installed [see here](https://github.com/python-poetry/poetry/pull/9333).
* virtme-ng: Open issue see [#107](https://github.com/arighi/virtme-ng/issues/107). Seems that there're some problems with zsh.

