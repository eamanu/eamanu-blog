---
title: My contributions to Free Software - May 2024
date: 31 May, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - May 2024

## Debian
* scikit-build-core: New upstream release 0.9.3. Uploaded to unstable.
* myst-parser: New upstream release 3.0.1. Uploaded to unstable.
* apispec: New upstream release 6.6.1. Uploaded to unstable.
* deap: Fix two important  (RC) bugs [#1018336](https://bugs.debian.org/1018336) and [#1070480](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1070480). Uploaded to [unstable](https://tracker.debian.org/news/1528240/accepted-deap-141-2-source-into-unstable/)
* ipyparallel: New upstream release 8.8.0. This upload will close these two RC bugs [#1061744](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1061744) and [#1063948](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1063948).
* pyramid_chameleon: Fix important bugs. Stop using nose for tests, and use pytest (see [#1018567](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1018567)). And two bugs relating to Python3.12 bulid (see [#1056499](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1056499) and [#1052780](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1052780)).
* scalene: New upstream release 1.5.41. Uploaded to unstable.
* python-plaster-pastedeploy: Fix RC bug [#1052860](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1052860) to build correctly the package with Python3.12. Fix minor bug [#1048274](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1048274) to build source after a successful build.
* mdit-py-plugins: New upstream release 0.4.1. Uploaded to unstable.
* python-tomlkit: New upstream release 0.12.5. Uploaded to unstable.
* poetry: New upstream release 1.8.3. Uploaded to unstable.
* monty: New upstream release 2024.5.15. Uploaded to unstable.
* fsspect: New upstream reelase 2024.5.0. Uploaded to unstable.
* monty: New upstream release 2024.5.25. Uploded to unstable.
* scikit-build-core: New upstream release 0.9.4. Uploaded to unstable.
* poetry-plugin-export: New upstream release 1.8.0. Uploaded to unstable.

### Sponsored Package
* circuits: New upstream release 3.2.3. Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.
* python-opt-einsum: Introduce to Debian version 3.3.0+dfsg-1. Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.
* python-proto-plus: Introduce to Debian version 1.23.0. Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.
* cozy: Introduce to Debian version 1.3.0. Kudos to Manuel Traut <manut@mecka.net>.

## Other projects
* Open an issue in HeadSetControl, I had a error during the build. For more details see [#344](https://github.com/Sapd/HeadsetControl/issues/344)
* Deap: Fix minor issue in documentation. See [PR](https://github.com/DEAP/deap/pull/746)
