---
author: eamanu
blogspot: true
date: Oct 11, 2018
tags: Old-Blog
title: "Gu\xEDa Debian Maintainer [2/?] \u2013 Documentos para leer"

---
# Guía Debian Maintainer [2/?] – Documentos para leer

Continuando con la lectura de la guía del [nuevo mantainer ](https://www.debian.org/doc/devel-manuals#maint-guide)este lista una serie de documentos que son de gran importancia y que deberían leerse:

- debian-policy (http://www.debian.org/doc/devel-manuals#policy)
- developer-reference (http://www.debian.org/doc/devel-manuals#devref)
- Autotools Tutorial (http://www.lrde.epita.fr/~adl/autotools.html)
- gnu-standards, este paquete contiene dos documentos del proyecto GNU que son importantes:
  - GNU Coding Standards (http://www.gnu.org/prep/standards/html\_node/index.html)
  - Information for Maintainers of GNU software (http://www.gnu.org/prep/maintain/html\_node/index.html)

Debian policy

Este es un documento, a mi parecer obligatorio, ya que describe resumidamente la estructura y contenido de los paquetes, como así también brinda una descripción del sistema operativo. Resume en unas pocas páginas el Filesystem Hierarchy Standard (http://www.debian.org/doc/packaging-manuals/fhs/fhs-2.3.html)

The Debian Free Software Guidelines

El DFSG hace su propia definción de software libre. La cual es:

- Libre Redistribución.
- Código Fuente: El programa debe incluir el codigo fuente, y este debe permitir su distribución.
- Trabajos derivados: La licencia de un programa debe permitir modificaciones y trabajos derivados.
- Integridad del Autor del codigo fuente.
- No discriminación contra otras personas o grupos. La licencia no debe imponer ningún tipo de discriminación.
- No debe discriminar ningún área de trabajo. Por ejemplo, el la licencia no debe restringir el uso del programa para el área de los negocios.
- Distribución de la licencia.
- La licencia no debe ser específica de debian
- La licencia no debe contaminar otros software.
- Ejemplos de licencias permitidas en Debian: «GPL», «BSD» y «Artistic»

Este documento también comenta sobre la existencia de tres diferentes «Archives» áreas:

- Main: Dónde se encuentran solo los paquetes que son considerados partes de la distribución. Todos los paquetes en esta área deben cumplir con la GFSG.
- Contrib: Contiene todos los paquetes que son sumplementarios y que trabajan con la distribución de Debian, pero necesitan de softwares externos para funcionar correctamente.
- non-free: Son paquetes complementarios que tratan de trabajar con la distribución de Debian pero que no cumplen al 100% con el GFSG o tiene algún otro problema de distribución.

También, este documento habla sobre los diferentes archivos que son necesarios a la hora de empaquetar un software. Ejemplos de estos archivos son: debian/changelog, debian/control, debian/copyright, debian/rules

En definitiva, estos documentos son casi obligatorios si se quiere contribuir en la empaquetación de Debian, o si se quiere saber como está estructurado.