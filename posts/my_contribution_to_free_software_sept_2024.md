---
title: My contributions to Free Software - September 2024
date: 30 Sep, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - September 2024

## Debian
* contourpy: New upstream release: 1.3.0.
* kpatch: Fix FTBFS in ppc64el arch, see [#1079032](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1079032).
* poetry-core: Fix RC bug
[#1081311](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1081311). This was an issue in poetry-core
because python3.12.6 change urlunsplit behaviour.
* poetry: Add a patch to possibly fix a  failed (apparently flaky) test that try to write in the sys.stderr. Uploaded  1.8.3+dfsg-4.
* fsspec: New upstream release: 2024.9.0. Ignore two tests tht failed. They need
  investigation. Forwarded:
  https://github.com/fsspec/filesystem_spec/issues/1677.
* pybind11: New upstream release 2.13.6. Uploaded to unstable.
* rust-obey: Introduce version 0.1.1 to Debian.
* rust-vec-collections: Introduce version 0.4.3 to Debian.
* rust-roots: Introduce version 0.0.8 to Debian.
* mdit-py-plugins: New upstream release 0.4.2.
* jupyter-packaging: Fix RC bug [#1080112](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1080112)


### Sponsored Package
* rust-core-maths: Introduce version 0.1.0 to Debian. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-ttf-parser: New upstream release 0.24.1. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-roxmltree: New upstream release 0.20. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-svg-metadata: new upstream release 0.5.0. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-dolby-vision: New upstream release 3.3.0. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-svgtypes: New upstream release 0.15.1. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-secular: Introduce version 1.0.1 to Debian. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-splitty: Introduce version 1.0.2 to Debian. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-char-reader: Introduce version 0.1.1 to Debian. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-custom-error: Introduce version 1.9.2 to Debian. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-unicode-bidi-mirroring: Introduce 0.3.0 to Debian. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-unicode-ccc: Introduce 0.3.0 to Debian. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-syntect-no-panic: Introduce version 4.6.1 to Debian. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-fontconfig-parser: Introduce version 0.5.7 to Debian. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-fontdb: Introduce version 0.21.0 to Debian. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-rustybuzz: Introduce version 0.17.0 to Debian. Kudos to NoisyCoil <noisycoil@tutanota.com>.
* rust-flex-grow: Introduce version 0.1.0 to Debian. Kudos to NoisyCoil <noisycoil@tutanota.com>.

## Other projects
poetry-core: Fix [#9678](https://github.com/python-poetry/poetry/issues/9678) through [PR#758](https://github.com/python-poetry/poetry-core/pull/758).

