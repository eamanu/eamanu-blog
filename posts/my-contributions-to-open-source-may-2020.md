---
author: eamanu
blogspot: true
date: Jun 06, 2020
tags: Old-Blog
title: "My contributions to Open Source \u2013 May 2020"

---
# My contributions to Open Source – May 2020

This will be a very very small entry. I couldn’t spent lot of time to free and open source projects.

I was working on the dateparser respository on salsa Debian. The repository didn’t fulfill with the DPMT policy (no pristine-tar and upstream branch) so I build them and push to Salsa. Also, I was working on the [\#954907](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=954907) Debian bug. The dateparser raise a FutureWarning during the build. I open this issue [\#688](https://github.com/scrapinghub/dateparser/issues/688) on upstream and this [PR699](https://github.com/scrapinghub/dateparser/pull/699) patch to solve it ( didn’t apply on Debian yet).

We worked on the release of Pycryptodome 3.9.7 on Debian with hntourne &lt;debian@nilux.br&gt; and josc &lt;josch@debian.org&gt;. There was several changes that you can see here: https://tracker.debian.org/news/1144399/accepted-pycryptodome-397dfsg1-1-source-into-unstable

I worked on the new upstream version of python-hypothesis 5-10.14 but now is on the the 5.16.0 version.

Also, worked on the new upstream release of anorack fixing: [\#961250](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=961250).

 Minor change on dateparser: https://github.com/scrapinghub/dateparser/pull/698

I start working on the Python documentation Spanish Translation https://github.com/python/python-docs-es making some translation.

Report this issue: https://github.com/eventoL/eventoL/issues/654

Also I was on the organization of the FLISOL (Festival Latinoamericano de Instalación de Software LIbre) this year the Festival was virtual for COVID-19 reason. But was great! (I will try to add an entry for FLISOL 2020 La Rioja.

Aims:

- Continue work on Python Documentation Translation
- Package Poetry on Debian: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=947261
- FLISOL La RIoja 2020 blog’s entry 🙂