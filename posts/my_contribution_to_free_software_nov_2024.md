---
title: My contributions to Free Software - November 2024
date: 30 Nov, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - November 2024

## Debian
* ariba: Fix FTBFS [#1085587](https://bugs.debian.org/1085587) in version
2.14.7+ds-4.
* python-whoosh: Fix Syntax Warning bug
  [#1078666](https://bugs.debian.org/1078666) and
  [#985017](https://bugs.debian.org/985017) on version
  2.7.4+git6-g9134ad92-9.
* mdtraj: Fix Python3.13 build [#1082092](https://bugs.debian.org/1082092)
* python-pybedtools: Fix bug [#1081673](https://bugs.debian.org/1081673) to
  build with Python3.13.
* zfec: Prepare a NMU to  Fix Python3.13 build
  [#1081480](https://bugs.debian.org/1081480). Version _1.5.7.4-0.2 uploaded.
* pygattlib: Prepare a NMU to Fix Python3.13 build
  [#1081650](https://bugs.debian.org/1081650). Version 0~20210616-1.1 uploaded.
* impacket: Ignore a test to fix RC [#1082142](https://bugs.debian.org/1082142).
  Version 0.12.0-3.
* pyflakes: Fix assertion error during the tests. Closes RC bug
  [#1082151](https://bugs.debian.org/1082151). Fix in version 3.2.0-2.
* python-deadlib: Introduce new package to get alive some dead battery from
  Python.


### Sponsored Package
* shotwell: New upstream release 0.32.10. Multiple bugs was closed: [#1078709](https://bugs.debian.org/1078709)[#1082015](https://bugs.debian.org/1082015)[#1083041](https://bugs.debian.org/1083041)[#1085721](https://bugs.debian.org/1085721). Kudos to Jörg Frings-Fürst <debian@jff.email>.


## Other projects
* pyflakes: Fix assertion error during tests. See issue
  [#812](https://github.com/PyCQA/pyflakes/issues/812). See PR [#821](https://github.com/PyCQA/pyflakes/pull/821).
   
