---
author: eamanu
blogspot: true
date: Jan 19, 2020
tags: Old-Blog
title: "My contributions to Open Source \u2013 October/December 2019"

---
# My contributions to Open Source – October/December 2019

2019 ends and I plan spend more time to FOSS on 2020. So here is my contributions to the end of 2019.

First of all, I can participate on PyConAr 2019 on December https://eventos.python.org.ar/events/pyconar2019/. PyConAr is the Python Conference on Argentina. This was my first PyConAR, so I was very excited. There, I participate like speaker where I talk about «Python on Debian: from the newbie’s point of view» (video: [https://www.youtube.com/watch?v=GGFpOMMCJpk&amp;list=PLtwHCKHTTq\_FCICEtCJkUmI0a4K-8N\_IT&amp;index=30&amp;t=0s](https://www.youtube.com/watch?v=GGFpOMMCJpk&list=PLtwHCKHTTq_FCICEtCJkUmI0a4K-8N_IT&index=30&t=0s)). My slides is on my [Github](https://github.com/eamanu/Talks/tree/master/2019/PyConAR).

![](../uploads/2020/01/LJT7342.jpg)On October I participate on the EILAR 2019 (Encuentro Informático Riojano) where thanks of the UNLaR (Universidad Nacional de La Rioja) I make two workshops: 1. Git workshop and 2. Debian packaging. Then I talk about the Free Software community en La Rioja (That I mention on https://github.com/eamanu/Talks/tree/master/2019/PyConAR) (Note for me: I have to write about the community :-)) I made a constructive criticism of the University, about the use and the courses of private software, that the University give.

I start (well, I try start) a startup called Yaerobi (yaerobi.com) to contribute and make open source software.

And in the other hand, I was (I am) focused on the Python 2 Removal Support on Debian. So I could learn a lot of things about Debian packaging and the different problems about that.

So, for this 2020 I plan several things on the FOSS environment:

- Become Debian Developer
- Contribute in a depth way to Python
- Make several courses on La Rioja, Argentina.
- Make growth the Free software Community in La Rioja.
- Create Open Data La Rioja
- Particpate on PyConAR 2020

Wishlist

- DebConf 2020
- PyCamp Argentina