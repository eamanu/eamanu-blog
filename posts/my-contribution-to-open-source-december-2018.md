---
author: eamanu
blogspot: true
date: Jan 02, 2019
tags: Old-Blog
title: "My contribution to open source \u2013 December 2018"

---
# My contribution to open source – December 2018

This month I to the next contributions:

- I reviewed 49 pull request on 2 repositories (This info is extract from Github)
- 

![](../uploads/2019/01/image.png)- I opened 7 pull request on 4 repositories:

![](../uploads/2019/01/image-1.png)- CPython:
  - Calling assertEquals for moderately long list takes too long (<https://bugs.python.org/issue19217>): open
  - bpo-35609: improve abc.py docstring (<https://bugs.python.org/issue35609>) This was an improve of the ABC’s docstring. This was closed because https://github.com/python/cpython/pull/11355 propose a better improve.
  - bpo-35616: Change references to ‘4.0’ (<https://bugs.python.org/issue35616>) propose of change reference version from 4.0 to 3.9. Still open.
- Newt:
  - Just littles improve on Readme.md of newt.
- scikit-learn:
  - DOC: fix document that fetch\_20newsgroups: easy issue to improve documentation.

Just 3 was merged and 2 are open up to date.

#### Activity on Debian

- The issue has #[2558](https://github.com/tornadoweb/tornado/issues/2558) been open on python/tornado. Originally, this issue was created by Chris Lamb (https://alioth-lists.debian.net/pipermail/python-modules-team/2018-December/051712.html) because the docs/conf.py on tonardo had a line that were not reproducible -&gt; *copyright = «2009-%s, The Tornado Authors» % time.strftime(«%Y»).* For this reason, I created the patch: https://salsa.debian.org/python-team/modules/python-tornado/commit/70526d2ad0022925b3f23f1b71e50928df1051f0 and open the issue on Tornado’s Github and the patch. This issue was taken and solved by upstream.

I upload to mentors.debian.net the next packages:

- sent – simple plaintext presentation tool: We are waiting for new bugs/upstream version to do adopt it officially.
- python-cassandra – Python driver for Apache Cassandra: New upstream version and close bug.
- python-impacket – Python module to easily build and dissect network protocols: new upstream version
- python-github{3} – Access to full Github API v3 from Python{2,3}: new upstream version.
- python-scp{3} – scp module for paramiko (Python {2,3}): New upstream version.
- python-pcapy{3} – Python interface to the libpcap packet capture library (Python {2,3}): new upstream version.