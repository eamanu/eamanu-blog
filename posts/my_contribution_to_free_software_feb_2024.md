---
title: My contributions to Free Software - February 2024
date: 29 February, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - February 2024

## Debian
* scalene: New upstream release 1.5.31.1. Closes 3 RC bugs: [#1037852](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1037852), [#1055725](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1055725), [#1056886](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1056886).
* ftputils: New upstream release 5.1.0.
* python-marshmallow-sqlalchemy: New upstream release 1.0.0.
* scikit-build-core: New upstream release 0.8.1.
* monty: New upstream release 2024.2.2.
* fsspec: New upstream release 2024.2.0
* pygithub: New upstream release 2.2.0
* python3-poetry-core: New upstream release 1.9.0. I uploaded to experimental, to test that it does not break any other build. Closes bug [#1049669](https://bugs.debian.org/1049669) to allow source rebuild binary packages without errors.
* gtts: Upload versio 2.5.1-1~bpo12+1 to bookworm-backports to fix RC bug [#1030290](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1030290).
* scalene: New upstream release 1.5.34.
* python-cassandra-driver: Stop run autopkgtest in s390x. Fix bug [#1064403](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1064403)
* poetry: Working in 1.8.0.dev0~git20240220.cff4d7d5+dfsg-1. As poetry-core is in version 1.9.0, this break the current poetry version in unstable. I package 1.8.0.dev0 and close the next bugs: [#1059356](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1059356), [#1029480](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1029480) and [#1058670](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1058670). For now uploaded to experimental.
* python-morris: Fix RC bug [#1026584](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1026584)(Python3.12 related work). It was a doctest failed test. And some minor improvements and updates in the package.

### Sponsored Package
* flask-restful: New upstream release 0.3.10 and fix issue in build (see [#1061028](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1061028)). Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.
* simplemonitor: Introduce version 1.12.0 to Debian. Kudos to Carles Pina i Estany <carles@pina.cat>.
* python-pylatex: Introduce package to Debian version 1.4.2. Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.
* python-redmine: New upstream release 2.4.0. Kudos to Akash Doppalapudi <akashdoppalapudi2001@gmail.com>.

## Other projects
* Python ninja-ide: Merge minor [PR](https://github.com/ninja-ide/ninja-ide/pull/2136.).

