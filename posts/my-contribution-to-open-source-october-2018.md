---
author: eamanu
blogspot: true
date: Nov 08, 2018
tags: Old-Blog
title: My contribution to open source &#8211; October 2018

---
# My contribution to open source &#8211; October 2018

During this month I had just a little contributions to open source, because a was working on the [SAOCOM 1A ](http://www.conae.gov.ar/index.php/espanol/misiones-satelitales/saocom/objetivos)mission [launching](https://www.youtube.com/watch?v=jsRYtrj-nZc).

In spite of this I participated on the the Hacktoberfest, and I can complete the challenge, so I am waiting to my new T-Shirt. Finish the Hackathon with the next contribution:

- 2 Contributions to [sunpy/sunpy](http://github.com/sunpy/sunpy)
- 2 Contributions to [jooaodanieel/GCommit](http://github.com/jooaodanieel/GCommit)
- 1 Contribution to [socialcopsdev/camelot](http://github.com/socialcopsdev/camelot)
- 2 Contributions to [ManuelGil/Hacktoberfest](http://github.com/ManuelGil/Hacktoberfest)
- 2 Contributions to [scikit-learn/scikit-learn](http://github.com/scikit-learn/scikit-learn)

The next month I will make a more detailed resume of my contributions!