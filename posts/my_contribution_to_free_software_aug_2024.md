---
title: My contributions to Free Software - August 2024
date: 31 Aug, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - August 2024

## Debian
* monty: New upstream release 2024.7.29. Uploaded to unstable.
* contourpy: New upstream release 1.2.1. Uploaded to unstable.
* myst-parser: New upstream release 4.0.0. Uploaded to unstable.
* python-scp: New upstrem release 0.15.0. Uploaded to unstable.
* python-scramp: New upstream release 1.4.5. Uploaded to unstable.
* tomlkit: New upstream release 0.13.0. Uploaded to unstable.
* scalene: New upstream release 1.5.43.2. Uploaded to unstable.
* scikit-build-core: New upstream release 0.10.3. Uploaded to unstable.
* python-blosc: New upsream release 1.11.2. Uploaded to unstable.
* python-tomlkit: New upstream release 0.13.2. Uploaded to unstable.
* docker-pycreds: New upstream release 0.4.0. Also, update debian files. Uploaded to unstable.
* python-marshmallow-sqlalchemy: New upstream release 1.1.0.  Uploaded to unstable.
* python-pyasn1-modules: New upstream release. Fix [#1051857](https://bugs.debian.org/1051857). Uploaded to unstable.
* scalene: New upstream release 1.5.44.1. Uploaded to unstable.
* scikit-build-core: New upstream release 0.10.5. Uploaded to unstable.
* scikit-build: New upstream release 0.18.1. Fix RC bug [#1079758](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1079758).

## DebConf24
* python-ring-doorbell: Sponsor new upstream release 0.8.12. Kudos to Carles Pina i Estany <carles@pina.cat>.
* simplemonitor: Sponsor new upstream release 1.13.0. Kudos to Carles Pina i Estany <carles@pina.cat>.
* python-requests-toolbelt: Fix RC bugs, there was tests that use internet. See [#1073442](https://bugs.debian.org/1073442)

### Sponsored Package


## Other projects
monty: create [issue](https://github.com/materialsvirtuallab/monty/issues/706) to note a unscryn between pypi version and githu repository.
monty: create a [PR](https://github.com/materialsvirtuallab/monty/pull/708) to add monty.os in the package.find section in pyproject.toml file.

