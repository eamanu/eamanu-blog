---
title: My contributions to Free Software - November 2023
date: 30 November, 2023
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - November 2023

## Debian
This is the list of the contributions:

* poetry: New upstream release 1.7.1. 
* python-xlrd: Promote python-xlrd 2.0.1-2 from experimental to unstable.

### Sponsored Package
* Yte: Uploaded.
* pyrgg: Review it.
* bdict: Upload 0.22.1. Fix fail for build with Python 3.12 (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1056230) and make package reproducible (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1055920).

### Rejuntada Debian Uruguay
* scikit-build-core: New upstream release 0.6.1.
* monty: New upstream release 2023.11.3.
* python-cleo: New upstream release 2.1.0.
* python-poetry-core: New upstream release 1.8.1.

#### Sponsored package
* python-scramp: Uploaded.
* python-cloudscraper: Uploaded
* python-ssdpy: Uploaded.

## Other projects
* Nothing this month :(
