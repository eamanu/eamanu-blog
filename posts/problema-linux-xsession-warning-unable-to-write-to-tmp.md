---
author: eamanu
blogspot: true
date: Jun 11, 2017
tags: Old-Blog
title: 'Problema Linux: Xsession: warning: unable to write to /tmp'

---
# Problema Linux: Xsession: warning: unable to write to /tmp

Hace algunos días tuve un problema con mi Debian (en Virtual Box). Utilizo maquinas virtuales para probar cosas nuevas, probar distrbuciones, o cuando estoy en una máquina que no me da privilegios de root y necesito trabajar con libertades.

Al trabajar en una máquina virtual, me doy ciertas libertades, como por ejemplo, que al momento de instalar el sistema operativo, le digo al instalador que a las particiones las haga automáticamente (Grave Error). Entonces, como la realización de las particiones ya viene preprogramadas, es algo posible que comenta algunos errores como, dar poca memoria a cosas importantes como la raiz («/»). Yo personalmente hago 3 particiones. La primera es la raiz, la cual dependiendo del disco que tenga le asigno (medio a ojo) la cantidad de memoria. Supongamos que tengo un disco de 1TB, le asignaría 100GB ( demasiado, pero ya me quedo tranquilo por un tiempo). Mi segunda partición es a Swap, la cual generalmente le asigno el doble de mi RAM. Si tengo 2GB de RAM, le asigno 4GB al swap.Y lo que queda le asigno al /home. Vi que muchos le asignan al /usr. Yo no lo hago.

Vamos al tema de este post. Un día como cualquier otro, quiero entrar a mi VM y todo parece normal. Pongo mi User y Pass y oh sorpresa! me sale un mensaje diciendome

```
Xsession: Warning: unable to write to /tmp
```

¿Qué es esto? Basicamente, por lo que investigué. El sistema operativo no tiene espacio para empezar a trabajar con el entorno gráfico. Vi soluciones tales como hacer los siguientes comandos

```
apt-get clean
apt-get autoclean
apt-get autoremove
```

Aparentemente esto daría solución a ese problemas. Pero para mi no lo fue. ¿ Y ahora?

Lo que hice fue algo rústico, y no se si es lo correcto, pero me funcionó. En primer lugar entre con la consola (Ctrl-Der + F2, estaba en Virtual Box por eso la combinación de teclas) y escribí el siguiente comando:

```
df -h
```

Y obtuve algo parecido a esto:

[![](../uploads/2017/06/Selection_007.png)](https://eamanu.com/blog/wp-content/uploads/2017/06/Selection_007.png)

Esto es de mi máquina personal (veo que mi /home está diciendome algo) pero en la máquina con el problema la línea que dice */dev/sda1* perteneciente al */* estaba hasta el 100%. Ahi estaba el problema.

Luego para comprobar que carpeta me estaba llenando la raíz hice (lleva su tiempo):

```
sudo du -sh /*
```

Y obtuve algo como se ve en la imágen. Vi que había mucha memoria en /usr.

[![](../uploads/2017/06/Selection_008.png)](https://eamanu.com/blog/wp-content/uploads/2017/06/Selection_008.png)

Entonces utilicé el siguiente comando:

```

du -a /usr | sort -n -r | head -n 20
```

Y me salió un top de todas las carpetas con más memoria utilizada. Vi que tenía un /usr/docs con 5 o 6 GB (No se que era realmente) lo unico que hice fue un sudo rm /usr/doc -R y listo! todo volvió a la normalidad.

Espero que alguien que tenga ese problema y esté leyendo esto le haya servido.

Referencias:  
https://bbs.archlinux.org/viewtopic.php?id=135835  
https://bbs.archlinux.org/viewtopic.php?id=96714  
https://www.experts-exchange.com/questions/26782995/Xsession-warning-unable-to-write-to-tmp.html