---
author: eamanu
blogspot: true
date: Aug 03, 2020
tags: Old-Blog
title: "My contributions to Open Source \u2013 June/July 2020"

---
# My contributions to Open Source – June/July 2020

On Debian I work a little in Poetry packaging reviewing a merge request. I worked on palettable https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=962340. Now is in pending upload.

Also I work in monty https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=962338, now I need sponsor.

Work on issue #963020 fixing a build error on impacket.

New upstream release of python-pgspecial (#955750), pygments, python-trezor (now I grant DM permissions), anorack.

Create a MR based on Geert Stappers patch https://salsa.debian.org/cpython-team/python3-defaults/-/merge\_requests/5

I work a little into python-docs-es reviewing an translating some pages.

First (very) easy commit on sphinx https://github.com/sphinx-doc/sphinx/commits?author=eamanu&amp;since=2020-06-15&amp;until=2020-06-16

I create this PR: https://github.com/scrapinghub/dateparser/pull/698 on dataparser.

I opened this issues

- https://github.com/materialsvirtuallab/monty/issues/142
- https://github.com/gisce/iec870ree/issues/20
- https://github.com/nccgroup/featherduster/issues/87
- https://github.com/python/python-docs-es/issues/483
- https://github.com/materialsvirtuallab/monty/issues/147
- https://github.com/python-poetry/poetry/issues/2607
- https://github.com/gisce/iec870ree/issues/22

In this year, the EuroPython was online, so I could participate on the Sprints. So, I start learn pip and warehouse. In this context I can create the next PR, some of that was already merged.

- https://github.com/pypa/warehouse/pull/8323
- https://github.com/pypa/pip/pull/8624
- https://github.com/pypa/pip/pull/8622

In the other hand, the meetup **Grupo de software libre y código abierto de La Rioja** was created: https://www.meetup.com/es-ES/Grupo-de-Software-Libre-y-Codigo-Abierto-de-La-Rioja/ and we had the first meet on 29th July 🙂 so I’m happy for this.

Also, I will participate on the DebCon20 as speaker. My short talk was accepted: https://debconf20.debconf.org/talks/55-an-experience-creating-a-local-community-in-a-small-town/. So now just need prepare it 😀