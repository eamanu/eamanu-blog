---
title: My contributions to Debian - February 2025
date: 28 Feb, 2025
author: eamanu
tags: new-blog, Debian, python, free-software
---

# My contribution to Debian - February 2025
* basemap: New upstream release 1.4.1. Close
  [#1074839](https://bugs.debian.org/1074839),
  [#1076957](https://bugs.debian.org/1076957) and
  [#1092778](https://bugs.debian.org/1092778).
  Uploaded to unstable.
* apispec: New upstream release 6.8.1. Uploaded to unstable.
* fsspec: New upstream release 2025.2.0. Uploaded to unstable.
* python-cassandra-driver: Fix RC Bug
  [#1098277](https://bugs.debian.org/1098277) in version 3.29.2-5. Thanks Bo Yu
  <vimer@debian.org> for the patch.
* myst-paser: New upstream release 4.0.1. Uploaded to unstable.
* poetry-core: New upstream release 2.1.1. Uploaded to experimental.



## Sponsored packages
* python-ring-doorbell: New upstream release 0.9.13. Kudos to Carles Pina i
  Estany <carles@pina.cat>.
* python-asyncclick: New upstream release 8.1.8.0+async. Kudos to Carles Pina i
  Estany <carles@pina.cat>.
* python-pbs-installer: Introduce version 2025.02.12 to Debian. Kudos to Boyuan
  Yang <byang@debian.org> to initial packaging and eevelweezel
  <eevel.weezel@gmail.com> for updated it to last upstream release.

## Other projects
