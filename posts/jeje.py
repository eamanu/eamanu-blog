import yaml
from dateutil.parser import parse
from pathlib import Path


class Header:
    def __init__(self, header):
        self.header = header

    def get_header_title(self):
        new_header = self._bump_header()
        return yaml.dump(new_header), new_header['title']

    def _bump_header(self):
        new_header = {}
        data = yaml.load(self.header, yaml.FullLoader)

        new_header['blogspot'] = True
        new_header['author'] = 'eamanu'

        date = parse(data['date'])
        new_header['date'] = date.strftime("%b %d, %Y")
        new_header['tags'] = 'Old-Blog'
        new_header['title'] = data['title']

        return new_header


class PostEntry:
    def __init__(self, pathname):
        self.pathname = pathname

    def get_new_entry(self):
        return self._new_entry()


    def _new_entry(self):
        with self.pathname.open() as f:
            data = f.read()

        data_spliter = data.split('---')
        header = Header(data_spliter[1])
        content = '\n'.join(data_spliter[2:])

        h, title = header.get_header_title()

        new_content = f'---\n{h}\n---\n# {title}\n{content}'

        return new_content

class Post:
    def __init__(self, input_file: Path, output_file: Path):
        self.input_path = input_file
        self.output_path = output_file

    def run(self):
        for md in self.input_path.glob('*.md'):
            file_str = PostEntry(md)
            output_file = self.output_path / md.name

            with output_file.open('w') as f:
                f.write(file_str.get_new_entry())


def main():
    p = Post(Path('old_blog/'), Path('old_blog_converted/'))
    p.run()


if __name__ == '__main__':
    main()
