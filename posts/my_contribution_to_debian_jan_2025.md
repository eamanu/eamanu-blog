---
title: My contributions to Debian - January 2025
date: 31 Jan, 2025
author: eamanu
tags: new-blog, Debian, python, free-software
---

Yes, I noted that my only contributions are to Debian Project, so the title changed :-). 

# My contribution to Debian - January 2025

* python-deadlib: Add Dependency of python3-audioop-lts for python3-standard-aifc package. Uploaded to unstable.
* python-cleo: Upload new upstream release to experimental version 2.2.1. After that uploaded to unstable.
* poetry-core: Upload new upstream release to unstable version 2.0.1.
* monty: Upload new usptream release to unstable version 2025.1.9.
* kineticstools: Fix RC [#1093350](https://bugs.debian.org/1093350) in version
  0.6.1+git20220223.1326a4d+dfsg-5.
* poetry-plugin-export: Upload new upstream realease to unstable version 1.9.0. 
* fsspec: Fix RC [#1092543](https://bugs.debian.org/1092543) in version
2024.12.0-2. Uploaded to unstable.
* kpatch: Fix RC [#1092202](http://bugs.debian.org/1092202) in version 0.9.9-6.
  Uploaded to unstable.
* python-flask-jwt-extended: Upload new usptream release 4.7.1.

## Other projects
* poetry-core: Fix a test that used a removed folder [see PR#820](https://github.com/python-poetry/poetry-core/pull/820).
