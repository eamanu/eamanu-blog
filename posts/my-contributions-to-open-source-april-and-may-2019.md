---
author: eamanu
blogspot: true
date: Jun 13, 2019
tags: Old-Blog
title: "My contributions to open source \u2013 April and May 2019"

---
# My contributions to open source – April and May 2019

Well this last two month I was a littler busy, so I can not log my open source activities. But yes, I was working on it 🙂

First of all, on the last month I will receive the notice that I was granted with a travel bursary to DebConf 2019. This year will be on Curitiba, Brasil. Curitiba is near of Argentina, so, for this year I will go to (and my first) DebConf (so excited!)

![](../uploads/2019/06/image.png)On May I started to working on CPython to deprecate loop parameters on Asyncio Public API. Meanwhile I make two PR:

- https://github.com/python/cpython/commit/ed9f3562b637a59b9000abbceee5ae369d35444d
- https://github.com/python/cpython/commit/a2fedd8c910cb5f5b9bd568d6fd44d63f8f5cfa5

One PR was merge to CPython:

- https://github.com/python/cpython/pull/13671

Also I was working on TLINDEN/note tool. I am the maintainer on Debian of that package, and I fix some typo to upstream:

- https://github.com/TLINDEN/note/commit/c5cdc4244550a6c5fa234ace66158bcacbea5723
- https://github.com/TLINDEN/note/commit/ec2a36b4ad8b8ac37e61cd23c73b18719c8855ee

I also, was looking the pythondotorg repository and I make the next commit:

- https://github.com/python/pythondotorg/commit/1342a54b9315e97a0c298b90e4e7124bddb308c9

On amzn/emukit I fix the issue: https://github.com/amzn/emukit/issues/197 on https://github.com/amzn/emukit/commit/80852eafc42f8de889dc77173365560ae2b502aa

And I make the next commit: https://github.com/SecureAuthCorp/impacket/commit/82f2447004ddc1fa0193a232b88a292a8c0f83d8 on SecureAuthCorp/impacket just to clean a little the code (remove comments)

Also I open 10 PR on 8 repository. One of them is escrutinio-social that I am thinking to write about that very interest project.

![](../uploads/2019/06/image-1.png)And I review 25 PRs

![](../uploads/2019/06/image-2.png)On April I participate on Flisol 2019 La Rioja, Argentina. I talk about «contributing to Python» https://github.com/eamanu/Talks/tree/master/2019/Flisol\_LaRioja. I am planned to write on a entry about this.

I made three commits to cpython:

- https://github.com/python/cpython/commit/86f0c8215c2fdaeef58d185ff00b854a45971f84
- https://github.com/python/cpython/commit/3993ccb6820d4239ce3d9e1c5d31f13b86e0000b
- https://github.com/python/cpython/commit/b00479d42aaaed589d8b374bf5e5c6f443b0b499

Also, I made two commits to pythondotorg

- https://github.com/python/pythondotorg/commit/e952408b4d1bbfae98cd6c62813f9103808b82b9
- https://github.com/python/pythondotorg/commit/710b4bbab30d429ce0177cbed8e300acfd7ec3e0

A commits on scikit-learn:

- https://github.com/scikit-learn/scikit-learn/commit/d88fec8d4b54cb76643d9f4647aca01dc4edff8d

And on celery

- https://github.com/celery/celery/commit/8c435dd525b06f2dacb1547c60ef0976538ac705