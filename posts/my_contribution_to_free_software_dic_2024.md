---
title: My contributions to Free Software - December 2024
date: 31 Dec, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---

# My contribution to Free Software - December 2024

## Debian
* apispec: New upstream release 6.7.1.
* apycula: New upstream release 0.15.
* contourpy: New upstream release 1.3.1.
* monty: New upstream release 2024.10.21.
* fsspec: New upstream release 2024.10.0. This release fix a RC bug [#1088301](https://bugs.debian.org/1088301),
  related to Python3.13 transition. Also this versions stop recommending
  obsolete fusepy [#1085592](https://bugs.debian.org/1085592). 
* qiime: Add patch to remove crypt module from tests. Not uploaded yet. Only
  push to salsa.
* pyqi: Fix SyntaxWarnings during the build
  [#1085822](https://bugs.debian.org/1085822) in version 0.3.2+dfsg-12.
* geneimpact: Add patch to fix SyntaxWarning during the build. Fix
  [#1085915](http://bugs.debian.org/1085915), in version 0.3.7-5.
* kpatch: Add
  [patch](https://salsa.debian.org/debian/kpatch/-/blob/debian/latest/debian/patches/fix-find-locals_syms.patch?ref_type=heads)
  to make it work in unstable. Upload version 0.9.9-5 to unstable.
* yanosim: Fix SyntaxWarning during the build
  [#1087194](https://bugs.debian.org/1087194).
* orderly-set: Introduce version 5.2.3 in Debian. Fix
  [#1089916](https://bugs.debian.org/1089916).
* python-pure-python-adb: Fix RC bug [#1082251](https://bugs.debian.org/1082251)
  to use python3-zombie-telnetlib. Also Fix SyntaxWarning during build bug
  [#1086921](https://bugs.debian.org/1086921)
* python-dicompylercore: Fix RC bug [#1090137](https://bugs.debian.org/1090137).
* audioop-lts: Introduce version 0.2.1 to Debian.
* monty: New upstream release 2024-12-10. Uploaded to unstable.
* rust-bisection: Introduce version 0.1.0 to Debian.
* pybedtools: Fix RC bug [#1090283](https://bugs.debian.org/1090283). Uploaded
  to unstable.
* rust-aync-http-content-range: Introduce version 0.2.0 to Debian.
* fsspec: New upstream release 2024.12.0. Uploaded to unstable.

### Sponsored Package
* keyring-pass: Introduce version _0.9.3 to Debian. Kudos to Bo YU <tsu.yubo@gmail.com>.
* python-libais: Fix RC bug [##1080133](https://bugs.debian.org/1080133). Kudos
  to Bo YU <tsu.yubo@gmail.com>.
* python-firebase-messaging: Introduce version 4.4.1 to Debian. Kudos to Carles
  Pina i Estany <carles@pina.cat>.

## Other projects
* fsspec: Open issue to get support to FUSE 3. See [#1759](https://github.com/fsspec/filesystem_spec/issues/1759)
* geneimpact: Open PR to fix syntaxWarning. See [#27](https://github.com/brentp/geneimpacts/pull/27)
* yanosim: Open PR to fix SyntaxWarning. See [#2](https://github.com/bartongroup/yanosim/pull/2)
* pure-python-adb: Open PR to fix SyntaxWanring. See [#115](https://github.com/Swind/pure-python-adb/pull/115)
