---
title: My contributions to Free Software - October 2023
date: 31 October, 2023
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - October 2023

## Debian
This is the list of the contributions:

* monty: New upstream release 2023.09.25.
* python-cassandra-driver: New upstream release 3.28.0.
* flask-jwt-extended: New upstrema release 4.5.3.
* aiodogstatsd: Fix broken symlink in documentation package. Fix bug [#1040600](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1040600).
* python-ldapdomaindump: New upstream release 0.9.4.
* fsspec: New upstream release 2023.9.2.
* shellingham: New upstream release 1.5.4.

### Sponsored uploads
* FileCheck: New in Debian. Closes: [#1052461|https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1052461].

## Other projects
* Nothing this month :(
