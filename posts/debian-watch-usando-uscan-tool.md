---
author: eamanu
blogspot: true
date: Aug 11, 2018
tags: Old-Blog
title: 'Debian/watch: Usando uscan Tool'

---
# Debian/watch: Usando uscan Tool

El archivo *watch* en el directorio debian de todo paquete debian (que valga la dedundancia) se utiliza para checkear si existen versiones más reciente del software empaquetado y lo descarga si es neceario. Para realizar una descarga necesitamos usar la herramienta uscan que forma parte del paquete devscript.

El archivo simplemente te dice: dónde descargar y te permite filtrar para poder obtener siempre la misma versión.

Hasta ahora todo bien. Todo sencillo ¿no?.

Primer el archivo tienen la siguiente forma:

<div class="wp-synhighlighter-outer" id="wpshdo_1"><div class="wp-synhighlighter-expanded" id="wpshdt_1"><table border="0" width="100%"><tr><td align="left" width="80%"><a name="#codesyntax_1"></a>[Source code](#codesyntax_1 "Click to show/hide code block")</td><td align="right">[![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/code.png)](#codesyntax_1 "Show code only") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/printer.png)](#codesyntax_1 "Print code") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/info.gif)](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/About.html "Show plugin information")</td></tr></table>

</div><div class="wp-synhighlighter-inner" id="wpshdi_1" style="display: block;">```
<pre class="make" style="font-family:monospace;">version<span class="sy0">=</span><span class="nu0">4</span>
http<span class="sy0">://</span>somesite<span class="sy0">.</span>com<span class="sy0">/</span>dir<span class="sy0">/</span>filename_<span class="br0">(</span><span class="sy0">.+</span><span class="br0">)</span><span class="sy0">.</span>tar<span class="sy0">.</span>gz
```

</div></div>Lo que hace uscan básicamente es entrar en la dirección web y «filtrar» según el wildcard que le escribamos.

Nunca viene mal una mirada al manual. Por ello dejo aquí el [manual](https://manpages.debian.org/stretch/devscripts/uscan.1.en.html)

Ok, vamos al grano. Generalmente, (bah), mejor dicho, *yo* casi siempre utilizo softwares que se encuetran almacenados en github o Pypi (que estos a su vez se encuentra en Github también.), por tal motivo voy a dejar un par de ejmplos de como se debería escribir el archivo watch para desgargar software desde Github, como así también de PyPi

Github




<div class="wp-synhighlighter-outer" id="wpshdo_2"><div class="wp-synhighlighter-expanded" id="wpshdt_2"><table border="0" width="100%"><tr><td align="left" width="80%"><a name="#codesyntax_2"></a>[Source code](#codesyntax_2 "Click to show/hide code block")</td><td align="right">[![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/code.png)](#codesyntax_2 "Show code only") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/printer.png)](#codesyntax_2 "Print code") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/info.gif)](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/About.html "Show plugin information")</td></tr></table>

</div><div class="wp-synhighlighter-inner" id="wpshdi_2" style="display: block;">```
<pre class="make" style="font-family:monospace;">version<span class="sy0">=</span>4
opts<span class="sy0">=</span>filenamemangle<span class="sy0">=</span>s<span class="sy0">/.+</span>\<span class="sy0">/</span>v?<span class="br0">(</span>\d\S<span class="sy0">+</span><span class="br0">)</span>\<span class="sy0">.</span>tar\<span class="sy0">.</span>gz<span class="sy0">/-</span><span class="re0">$1</span>\<span class="sy0">.</span>tar\<span class="sy0">.</span>gz<span class="sy0">/</span> \
https<span class="sy0">://</span>github<span class="sy0">.</span>com<span class="sy0">///</span>tags <span class="sy0">.*/</span>v?<span class="br0">(</span>\d\S<span class="sy0">+</span><span class="br0">)</span>\<span class="sy0">.</span>tar\<span class="sy0">.</span>gz
```

</div></div>Pypi

-

Para Pypi el archivo será muy parecido:

<div class="wp-synhighlighter-outer" id="wpshdo_3"><div class="wp-synhighlighter-expanded" id="wpshdt_3"><table border="0" width="100%"><tr><td align="left" width="80%"><a name="#codesyntax_3"></a>[Source code](#codesyntax_3 "Click to show/hide code block")</td><td align="right">[![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/code.png)](#codesyntax_3 "Show code only") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/printer.png)](#codesyntax_3 "Print code") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/info.gif)](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/About.html "Show plugin information")</td></tr></table>

</div><div class="wp-synhighlighter-inner" id="wpshdi_3" style="display: block;">```
<pre class="make" style="font-family:monospace;">version<span class="sy0">=</span>4
opts<span class="sy0">=</span>pgpsigurlmangle<span class="sy0">=</span>s<span class="sy0">/</span><span class="re0">$/</span><span class="sy0">.</span>asc<span class="sy0">/</span> \
https<span class="sy0">://</span>pypi<span class="sy0">.</span>debian<span class="sy0">.</span>net<span class="sy0">//-</span><span class="br0">(</span><span class="sy0">.+</span><span class="br0">)</span>\<span class="sy0">.</span><span class="br0">(</span>?<span class="sy0">:</span>zip<span class="sy0">|</span>tgz<span class="sy0">|</span>tbz<span class="sy0">|</span>txz<span class="sy0">|</span><span class="br0">(</span>?<span class="sy0">:</span>tar\<span class="sy0">.</span><span class="br0">(</span>?<span class="sy0">:</span>gz<span class="sy0">|</span>bz2<span class="sy0">|</span>xz<span class="br0">)</span><span class="br0">)</span><span class="br0">)</span>
```

</div></div>Lo de PyPi es que podemos utilizar la siguiente página para tener el correcto upstrem para descargar:

http://pypi.debian.net//watch

Vamos a la práctica
===================

En mi caso estoy tratano de empaquetar un módulo de python que se llama «[pcapy](https://www.coresecurity.com/corelabs-research/open-source-tools/pcapy)«. Entre los varios update que tengo que realizar, uno de estos es el watch file. Esto se debe a que Lintian me devuelve el siguiente error:

> Problems while searching for a new upstream version
> 
> uscan had problems while searching for a new upstream version:
> 
> In debian/watch no matching files for watch line  
> http://corelabs.coresecurity.com/index.php?module=Wiki&amp;action=view&amp;type=tool&amp;name=Pcapy /index\\.php\\?module=Wiki&amp;action=attachment&amp;type=tool&amp;page=Pcapy&amp;file=pcapy-(\[0-9.\]+)\\.tar\\.gz

Por lo visto hay algo que no le gusta a Lintian. Por lo tanto me cloné el proyecto a mi máquina. Una vez realizado esto, le digo a uscan, descarga si tenes alguna versión nueva. Para ello uso el siguiente comando:

<div class="wp-synhighlighter-outer" id="wpshdo_4"><div class="wp-synhighlighter-expanded" id="wpshdt_4"><table border="0" width="100%"><tr><td align="left" width="80%"><a name="#codesyntax_4"></a>[Source code](#codesyntax_4 "Click to show/hide code block")</td><td align="right">[![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/code.png)](#codesyntax_4 "Show code only") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/printer.png)](#codesyntax_4 "Print code") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/info.gif)](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/About.html "Show plugin information")</td></tr></table>

</div><div class="wp-synhighlighter-inner" id="wpshdi_4" style="display: block;">```
<pre class="make" style="font-family:monospace;">uscan <span class="sy0">-</span>dd <span class="sy0">-</span>v
```

</div></div>La opción -dd fuerza a uscan a descargar la versión filtrada. -v es el verbose.

Esto me duevuelve lo siguiente:

<div class="wp-synhighlighter-outer" id="wpshdo_5"><div class="wp-synhighlighter-collapsed" id="wpshdt_5"><table border="0" width="100%"><tr><td align="left" width="80%"><a name="#codesyntax_5"></a>[Source code](#codesyntax_5 "Click to show/hide code block")</td><td align="right">[![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/code.png)](#codesyntax_5 "Show code only") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/printer.png)](#codesyntax_5 "Print code") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/info.gif)](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/About.html "Show plugin information")</td></tr></table>

</div><div class="wp-synhighlighter-inner" id="wpshdi_5" style="display: none;">```
<pre class="text" style="font-family:monospace;">uscan info: uscan (version 2.17.6+deb9u1) See uscan(1) for help
uscan info: Scan watch files in .
uscan info: Check debian/watch and debian/changelog in .
uscan info: package="pcapy" version="0.10.8-2" (as seen in debian/changelog)
uscan info: package="pcapy" version="0.10.8" (no epoch/revision)
uscan info: ./debian/changelog sets package="pcapy" version="0.10.8"
uscan info: Process ./debian/watch (package=pcapy version=0.10.8)
uscan info: opts: filenamemangle=s/.*=(.*)/$1/
uscan info: line: http://corelabs.coresecurity.com/index.php?module=Wiki&action=view&type=tool&name=Pcapy /index\.php\?module=Wiki&action=attachment&type=tool&page=Pcapy&file=pcapy-([0-9.]+)\.tar\.gz
uscan info: Parsing filenamemangle=s/.*=(.*)/$1/
uscan info: line: http://corelabs.coresecurity.com/index.php?module=Wiki&action=view&type=tool&name=Pcapy /index\.php\?module=Wiki&action=attachment&type=tool&page=Pcapy&file=pcapy-([0-9.]+)\.tar\.gz
uscan info: Last orig.tar.* tarball version (from debian/changelog): 0.10.8
uscan info: Last orig.tar.* tarball version (dversionmangled): 0.10.8
uscan info: Requesting URL:
http://corelabs.coresecurity.com/index.php?module=Wiki&action=view&type=tool&name=Pcapy
uscan info: redirections: https://www.coresecurity.com/corelabs-research/open-source-tools/pcapy
uscan info: Matching pattern:
(?:(?:http://corelabs.coresecurity.com)?\/index\.php\?module\=Wiki\&action\=view\&type\=tool\&name\=Pcapy)?/index\.php\?module=Wiki&action=attachment&type=tool&page=Pcapy&file=pcapy-([0-9.]+)\.tar\.gz (?:(?:https://www.coresecurity.com)?\/corelabs\-research\/open\-source\-tools\/pcapy)?/index\.php\?module=Wiki&action=attachment&type=tool&page=Pcapy&file=pcapy-([0-9.]+)\.tar\.gz (?:(?:https://www.coresecurity.com)?\/corelabs\-research\/open\-source\-tools\/)?/index\.php\?module=Wiki&action=attachment&type=tool&page=Pcapy&file=pcapy-([0-9.]+)\.tar\.gz
uscan warn: In debian/watch no matching files for watch line
http://corelabs.coresecurity.com/index.php?module=Wiki&action=view&type=tool&name=Pcapy /index\.php\?module=Wiki&action=attachment&type=tool&page=Pcapy&file=pcapy-([0-9.]+)\.tar\.gz
uscan info: Scan finished
```

</div></div>Evidentemente hay algo mal en el archivo watch. Para ello, no lo voy a pensar demasiado y voy a usar la página de PyPi. http://pypi.debian.net/pcapy/watch

Y el resultado fue:

> **502 Bad Gateway**

Ok. Voy a ver que me muestra Github: https://github.com/CoreSecurity/pcapy/releases

Aquí se puede observar que se encuentran las releases y que está actualizada. Por lo tanto voy a apuntar mi archivo watch a los release de pcapy en Github:

<div class="wp-synhighlighter-outer" id="wpshdo_6"><div class="wp-synhighlighter-expanded" id="wpshdt_6"><table border="0" width="100%"><tr><td align="left" width="80%"><a name="#codesyntax_6"></a>[Source code](#codesyntax_6 "Click to show/hide code block")</td><td align="right">[![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/code.png)](#codesyntax_6 "Show code only") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/printer.png)](#codesyntax_6 "Print code") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/info.gif)](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/About.html "Show plugin information")</td></tr></table>

</div><div class="wp-synhighlighter-inner" id="wpshdi_6" style="display: block;">```
<pre class="make" style="font-family:monospace;">version<span class="sy0">=</span>4
opts<span class="sy0">=</span>filenamemangle<span class="sy0">=</span>s<span class="sy0">/.+</span>\<span class="sy0">/</span>v?<span class="br0">(</span>\d\S<span class="sy0">+</span><span class="br0">)</span>\<span class="sy0">.</span>tar\<span class="sy0">.</span>gz<span class="sy0">/</span>pcapy<span class="sy0">-</span><span class="re0">$1</span>\<span class="sy0">.</span>tar\<span class="sy0">.</span>gz<span class="sy0">/</span> \
https<span class="sy0">://</span>github<span class="sy0">.</span>com<span class="sy0">/</span>CoreSecurity<span class="sy0">/</span>pcapy<span class="sy0">/</span>tags <span class="sy0">.*/</span>v?<span class="br0">(</span>\d\S<span class="sy0">+</span><span class="br0">)</span>\<span class="sy0">.</span>tar\<span class="sy0">.</span>gz
```

</div></div>Veamos ahora que suecede si ejecuto uscan -dd -v:

<div class="wp-synhighlighter-outer" id="wpshdo_7"><div class="wp-synhighlighter-collapsed" id="wpshdt_7"><table border="0" width="100%"><tr><td align="left" width="80%"><a name="#codesyntax_7"></a>[Source code](#codesyntax_7 "Click to show/hide code block")</td><td align="right">[![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/code.png)](#codesyntax_7 "Show code only") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/printer.png)](#codesyntax_7 "Print code") [![](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/themes/default/images/info.gif)](https://eamanu.com/blog/wp-content/plugins/wp-synhighlight/About.html "Show plugin information")</td></tr></table>

</div><div class="wp-synhighlighter-inner" id="wpshdi_7" style="display: none;">```
<pre class="text" style="font-family:monospace;">uscan -dd -v
uscan info: uscan (version 2.17.6+deb9u1) See uscan(1) for help
uscan info: Scan watch files in .
uscan info: Check debian/watch and debian/changelog in .
uscan info: package="pcapy" version="0.10.8-2" (as seen in debian/changelog)
uscan info: package="pcapy" version="0.10.8" (no epoch/revision)
uscan info: ./debian/changelog sets package="pcapy" version="0.10.8"
uscan info: Process ./debian/watch (package=pcapy version=0.10.8)
uscan info: opts: filenamemangle=s/.+\/v?(\d\S+)\.tar\.gz/pcapy-$1\.tar\.gz/
uscan info: line: https://github.com/CoreSecurity/pcapy/tags .*/v?(\d\S+)\.tar\.gz
uscan info: Parsing filenamemangle=s/.+\/v?(\d\S+)\.tar\.gz/pcapy-$1\.tar\.gz/
uscan info: line: https://github.com/CoreSecurity/pcapy/tags .*/v?(\d\S+)\.tar\.gz
uscan info: Last orig.tar.* tarball version (from debian/changelog): 0.10.8
uscan info: Last orig.tar.* tarball version (dversionmangled): 0.10.8
uscan info: Requesting URL:
https://github.com/CoreSecurity/pcapy/tags
uscan info: Matching pattern:
(?:(?:https://github.com)?\/CoreSecurity\/pcapy\/tags)?.*/v?(\d\S+)\.tar\.gz
uscan info: Found the following matching hrefs on the web page (newest first):
/CoreSecurity/pcapy/archive/0.11.4.tar.gz (0.11.4) index=0.11.4-1
/CoreSecurity/pcapy/archive/0.11.3.tar.gz (0.11.3) index=0.11.3-1
/CoreSecurity/pcapy/archive/0.11.2.tar.gz (0.11.2) index=0.11.2-1
/CoreSecurity/pcapy/archive/0.11.1.tar.gz (0.11.1) index=0.11.1-1
/CoreSecurity/pcapy/archive/0.11.0.tar.gz (0.11.0) index=0.11.0-1
/CoreSecurity/pcapy/archive/0.10.10.tar.gz (0.10.10) index=0.10.10-1
/CoreSecurity/pcapy/archive/0.10.9.tar.gz (0.10.9) index=0.10.9-1
/CoreSecurity/pcapy/archive/0.10.8.tar.gz (0.10.8) index=0.10.8-1
uscan info: Matching target for downloadurlmangle: https://github.com/CoreSecurity/pcapy/archive/0.11.4.tar.gz
uscan info: Upstream URL (downloadurlmangled):
https://github.com/CoreSecurity/pcapy/archive/0.11.4.tar.gz
uscan info: Newest upstream tarball version selected for download (uversionmangled): 0.11.4
uscan info: Matching target for filenamemangle: /CoreSecurity/pcapy/archive/0.11.4.tar.gz
uscan info: Download filename (filenamemangled): pcapy-0.11.4.tar.gz
uscan: Newest version of pcapy on remote site is 0.11.4, local version is 0.10.8
uscan: => Newer package available from
https://github.com/CoreSecurity/pcapy/archive/0.11.4.tar.gz
uscan info: Downloading upstream package: pcapy-0.11.4.tar.gz
uscan info: Requesting URL:
https://github.com/CoreSecurity/pcapy/archive/0.11.4.tar.gz
uscan info: Successfully downloaded package: pcapy-0.11.4.tar.gz
uscan info: Start checking for common possible upstream OpenPGP signature files
uscan info: End checking for common possible upstream OpenPGP signature files
uscan info: Missing OpenPGP signature.
uscan info: New orig.tar.* tarball version (oversionmangled): 0.11.4
uscan info: Executing internal command:
mk-origtargz --package pcapy --version 0.11.4 --compression gzip --directory .. --copyright-file debian/copyright ../pcapy-0.11.4.tar.gz
uscan info: New orig.tar.* tarball version (after mk-origtargz): 0.11.4
uscan info: Successfully symlinked ../pcapy-0.11.4.tar.gz to ../pcapy_0.11.4.orig.tar.gz.
uscan info: Scan finished
```

</div></div>voilà!

Ya tengo descargada la última versión de pcapy.

> drwxr-xr-x 6 eamanu eamanu 4096 Aug 10 21:58 pcapy  
> lrwxrwxrwx 1 eamanu eamanu 19 Aug 10 23:17 pcapy\_0.11.4.orig.tar.gz -&gt; pcapy-0.11.4.tar.gz  
> -rw-r–r– 1 eamanu eamanu 35623 Aug 10 23:17 pcapy-0.11.4.tar.gz

Listo, es todo por hoy. En esta entrada mostré (muy, pero muy brevemente) como se debería trabajar con uscan, para que sirve. Ojalá en el futuro pueda dar un update y profundizar un poco más en esta tool