---
title: My contributions to Free Software - January 2023
date: 31 January, 2023
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - January 2023

## Debian
This is the list of the contributions:

* python-cleo: New upstream release 2.0.1. Uploaded to [unstable](https://tracker.debian.org/news/1407523/accepted-python-cleo-201-3-source-all-into-unstable/).
* python-xlib: New upstream release 0.33. Uploaded to unstable by [Jeroen Ploemen](https://tracker.debian.org/news/1405859/accepted-python-xlib-033-1-source-into-unstable/).
* poetry-core: New upstream release 1.4.0. Uploaded to [unstable](https://tracker.debian.org/news/1407522/accepted-poetry-core-140-3-source-into-unstable/).
* poetry: New upstream release 1.3.2. Uploaded to [unstable](https://tracker.debian.org/news/1410785/accepted-poetry-132dfsg-3-source-into-unstable/)
* Apply python-xlib [patch](https://github.com/python-xlib/python-xlib/pull/242) that affect the build of [herbstluftwm](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1028826). Uploaded to unstable by [Jeroen Ploemen](https://tracker.debian.org/news/1414946/accepted-python-xlib-033-2-source-into-unstable/).
* scikit-build: New upstream release 0.16.6. Uploaded to unstable by [Stefano Rivera](https://tracker.debian.org/news/1415057/accepted-scikit-build-0164-1-source-into-unstable/). Seconds attempt after issues in the autopkgtest test.

# Other projects
Nothing :(
