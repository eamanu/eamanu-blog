---
title: Poetry package in Debian
date: 25  February, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# Poetry package in Debian [2]

If you have any comments or recommedantion or opinion just ping me to eamanu@debian.org. I will respond you as soon as possible.

Let's talk a little bit of python3-poetry-core. This package is the core of poetry package. Poetry-core is a pep517 build backend. In stable the version is 1.4.0,  in unstable and testing the version is 1.8.1 (up to date) but I upload yesterday to experimental the version 1.9.0. If I want to move to unstable first I need to package poetry to version 1.8.0. But for now poetry 1.8.0 is not released the last version for now is 1.8.0.dev0. I will create a release plan for poetry, because I want that poetry dev release go first to experimental. But I have the dilema because poetry-core 1.9.0 is now released and is not compatible with the last version of poetry. IMHO they should be independent at least do not have a strict dependency between versions. Now I should upload to unstable development version that could break something from people that use testing.

Now poetry-core 1.9.0 is in experimental waiting for poetry 1.8.0.dev0 (that is already in experimental). What is the new in the Debian package? Well, I used some work from Simon Josefsson (https://salsa.debian.org/salsa-ci-team/pipeline/-/merge_requests/470) and I started to build the reverse dependency of poetry-core in Salsa to reduce the possibility to break package in unstable and work better before uploads. You can see the pipeline [here](https://salsa.debian.org/python-team/packages/poetry-core/-/pipelines/643905) I modified a little bit the pipeline to fix some issues that I found (see [here](https://salsa.debian.org/eamanu/pipeline-b-reverse/-/raw/build-reverse-dependencies/salsa-ci.yml)).

There are some packages that failed during the build, but they failed during the tests, I would be worried if the packages fail during the build itself.

In the next days I plan upload poetry and poetry-core to unstable. I would like to have develop releases (in experimental) of poetry weekly. In this way I can have a poetry build (perhaps poetry-core as well) and maybe have some feedback to upstream.

I hope write another blog entry next week. See you!