---
author: eamanu
blogspot: true
date: Feb 03, 2021
tags: Old-Blog
title: "My contributions to Open Source - January 2021"

---
# My contributions to Open Source – January 2021

After a week for holidays, now it’s time to start contribute 🙂

Debian



- Working in python3-pylev packaging: Accepted to unstable. (Thanks Jonathan Carte for sponsorship)
- Working in python3-crashtest packaging: Go to NEW (Thanks Stefano Rivera for sponsorship)
- Working on Poetry packaging. Still in progress.
- Working on python3-clikit packaging: Go to NEW (Thanks Stefano Rivera for sponsorship)
- Working on python3-aiodogstatsd: working on package documentation and new upstream release. Still in progress.
- Working on the adoption of the cuyo game: https://bugs.debian.org/916692
- Working in python3-shellingham packaging: reviewed by Stefano Rivera, still in progress.
- Add autopkgtest to python3-cachy, and some other updates.
- Fix [\#980473](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=980473) on python3-potr.
- Working on the adoption of engima game. https://bugs.debian.org/902855
- Working in python3-cleo packaging: still in progress.

Open source



--

- Minor PR for x11docker https://github.com/mviereck/x11docker/commit/e1ff1dd41d0a2d2ffa92eaf72251b35b5b87fed0 updating the License year.
- Start discussion on Sopel about the way that the upstrea create the releases: https://github.com/sopel-irc/sopel/issues/2022 based on this bug on Debian: [\#943405](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=943405).
- Open an simple issue on gnome-pomodoro [\#544](https://github.com/gnome-pomodoro/gnome-pomodoro/issues/544).
- Review pull requests on python-docs-es.
- Review a few pull requests on cpython.

Kernel




Send my first patch kernel. Under discussion. Very very very minor patch

- https://lkml.org/lkml/2021/1/29/429
