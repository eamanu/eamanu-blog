---
author: eamanu
blogspot: true
date: Feb 06, 2019
tags: Old-Blog
title: "My contribution to open source \u2013 January 2019"

---
# My contribution to open source – January 2019

This is a new year, and like happen always on new years we purpose several goals. My principal goal is have more contribution to CPython. I want to in the future convert me in a Core Dev (I know this is a hard work), maybe not in this year but yes on 2020. My other goal is convert me in a Debian Developer, so I have to involve more on Debian too. I want to involve me on contribute on Cython and Django projects.

So, let’s see my contributions to open sources on this month (a short month because I was on holiday).

- I had my first PR accepted on CPython (so, now I can say that I am a contributor ;-)). The PR is: [bpo-35641: IDLE – calltip not properly formatted if no docstring:](https://github.com/python/cpython/pull/11415) *IDLE: calltips not properly formatted for functions without doc-str*ings <https://bugs.python.org/issue35641>
- Now, on Debian, I am maintainer of note<https://github.com/TLINDEN/note> a perl module to create notes on command line. So, this have some spelling problems, for this reason I create the PR: <https://github.com/TLINDEN/note/pull/6> is open yet.
- I created on CPython two PR, they are under review:
  - [bpo-24928: Add test case for patch.dict using OrderedDict](https://github.com/python/cpython/pull/11437)
  - [bpo-35685: Add examples of unittest.mock.patch.dict usage](https://github.com/python/cpython/pull/11456)
- Now, I am helping on some issue on [reportabug](https://github.com/eamanu/reportabug) project. This is a tool that create reports when a Python module fail.
- I make 58 PR review on three repository: CPython (56), scikit-learn (1) y Python/devguide (1)

On Debian I:

- For now, I just adopt [note](https://tracker.debian.org/pkg/note) package. For now the version is 1.3.26-2. I am preparing a 1.3.26-3 that have patch with fixing some issues.