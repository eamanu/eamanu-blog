---
author: eamanu
blogspot: true
date: Mar 02, 2020
tags: Old-Blog
title: Soy Debian Maintainer

---
# Soy Debian Maintainer

Después de un largo camino que empezó en el año 2017 (yeah!), cuando un día me despertó la curiosidad de cómo sería comenzar a contribuir en algún proyecto de Open Source, me uní a Debian, contribuyendo de a poco.

Cuando recien se comienza a contribuir en el mundo de Debian, el status que se lleva es de Debian Contributor. Obviamente, hay que tener tiempo, para dedicarle a este mundo, por ello hubiera sido mejor empezar en este mundo durante mi época de estudiante. Luego de haber contribuido, y de demostrar cierto interes en el proyecto, llega el tan ansiado «advocate» para cambiar el status, en este caso a DM (Debian Maintainer)

Así que me llegó el advocate de JCC (<https://nm.debian.org/person/jcc>) e inicié el proceso para DM <https://nm.debian.org/process/710>.

Oficialmente soy DM desde 2020-02-15. Ahora, próximo objetivo Debian Developer 🙂

My key: 13796755BBC72BB8ABE2AEB5FA9DEC5DE11C63F1