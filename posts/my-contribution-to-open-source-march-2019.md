---
author: eamanu
blogspot: true
date: Apr 22, 2019
tags: Old-Blog
title: "My contribution to open source \u2013 March 2019"

---
# My contribution to open source – March 2019

![](../uploads/2019/04/image.png)On Github:

- I created 5 commits on CPython:
  - [bpo-36385: Add «elif« sentence on to avoid multiple «if«](https://github.com/python/cpython/commit/ed5e29cba500c2336aacdb7c77953f1064235b72)
  - [bpo-36185: Fix typo in Doc/c-api/objbuffer.rst. ](https://github.com/python/cpython/commit/ecc161d1209bf6d21f0fd6bef28476eda7cdaf79)
  - [bpo-36209: Fix typo on hashlib error message](https://github.com/python/cpython/commit/b71e28ea91259ca3914e2ff84fc126795ea6b848)
  - [bpo-36377: Specify that range() can not be compared](https://github.com/python/cpython/pull/12468)
  - [bpo-20582: add link for manpage for flags on getnameinfo()](https://github.com/python/cpython/pull/11977)
- I created 2 commits to reportabug:
  - [Delete unnecessary imports](https://github.com/zooba/reportabug/commit/e82188830fce609c2296e131e4cd96af14712265)
  - [Remove machine if already part of the platform string](https://github.com/zooba/reportabug/commit/a1fa1ebb566d36f21998a993c3ef57eecb421a35)
- I created an issue on flask:
  - [Problem on installation from pip](https://github.com/JeffCarpenter/flask-bootstrap4/issues/1)
- I make 55 review:
  - 54 review on CPython
  - 1 on python/devguide

![](../uploads/2019/04/image-2.png)