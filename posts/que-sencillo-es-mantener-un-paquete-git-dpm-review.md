---
author: eamanu
blogspot: true
date: May 03, 2018
tags: Old-Blog
title: "\xA1Que sencillo es mantener un paquete! git-dpm review"

---
# ¡Que sencillo es mantener un paquete! git-dpm review

Hace un tiempo que estoy contribuyendo en el proyecto Debian, en mis tiempos libres, espero algún día poder dedicarle más tiempo. Me parece que es un gran proyecto para contribuir, hay miles de personas allí y siempre están dispuestas a ayudar.

Pero para comenzar a contribuir, se necesita un poco de fuerza de voluntad (vamos, no todo es color de rosa). Mucha documentación, talvez no tan clara como para un principiante, pero hay documentos para todo y en una gran cantidad. Lo más complicado para mi, fue entender cuál es el proceso para tomar un parquete que está «disponible» (si quieres subir tus propios desarrollos es otra hisoria) hasta empaquetarlo, modificando todos los files necesarios de debian. Para ello, se utilizan varias herramientas, dquilt, uscan, uupdate, apt-get, etc. cada una de las cuales tiene sus propias funciones. Pero la que me pareció sumamente interesante, fue la que me mostró uno de mis mentores Filip (https://twitter.com/genunix) es la herramienta git-dpm (https://wiki.debian.org/PackagingWithGit/GitDpm).

Me encontré en la situación de que tengo un paquete que estoy manteniendo (pygithub) y salió una nueva versión del upstream. Como está configurado el file debian/watch, para estar observando nuevas actualizaciones, dentro de mi proyecto git del paquete, simplemente ejecuto el comando uscan. Esto hace que se descargue el orig.tar.gz de la nueva versión en el folder ../

Es decir me queda la estructura así:  
|\_\_pygithub/  
|\_\_pygithub\_1.40a3.orig.tar.gz  
|\_\_PyGithub-1.40a3.tar.gz

Ya con esto dentro de la carpeta pygithub, ejecuto el comando

`git-dpm import-new-upstream --rebase ../pygithub_1.40a3.orig.tar.gz`

Luego, hay que seguir las órdenes de git-dpm, pero en resúmen los comando que ejecuté para tener el paqueto listo para ser enviado fueron:

`<br></br>git-dpm update-patches<br></br>pristine-tar commit ../pygithub_1.40a3.orig.tar.gz 87ba13c22c314ba3b11635dd5cb33b12b0dd6800<br></br>git push`

Lo que más me llama la atención es que el mismo comando te va indicando los pasos a seguir.

Espero que le sirva a quien haya leido esto. Es solo un resúmen.