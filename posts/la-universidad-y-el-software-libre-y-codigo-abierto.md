---
author: eamanu
blogspot: true
date: Feb 17, 2020
tags: Old-Blog
title: "La Universidad y el Software Libre y C\xF3digo Abierto"

---
# La Universidad y el Software Libre y Código Abierto

El año pasado para el Encuentro Informático de La Rioja (2019), que tengo la suerte de venir participando desde el 2012, hice esta pequeña crítica que hoy estoy escribiendo aquí.

En realidad, es un crítica a algunas políticas universitarias (o departamentales más propiamente dicha), que tal vez no esté tan plasmadas por escrito, si no más bien naturalizada, pero que están allí y las tengo que mostrar. Por lo tanto, no creo que sean desiciones tomadas adrede, si no más bien algo natural, que ante el desconocimiento o simplemente por seguir la corriente de lo que la «industria» busca obtener de los estudiantes y egresados de las Universidades, las toman.

Mi duda sobre el rol del Software Libre en la Universidad Pública surgió en el preciso momento en el que entré a «chusmear» los cursos que lleva a cabo la Escuela de Informática de La Universidad Nacional de La Rioja. Con lo que me encuentro con la siguiente información:

![](../uploads/2020/02/excel.png)![](../uploads/2020/02/excel3.png)![](../uploads/2020/02/msproject.png)![](../uploads/2020/02/excel4.png)![](../uploads/2020/02/image.png)Obviamente no son lo únicos cursos que se dictan, podemos encontrar talleres sobre Android, PHP y Arduino. Pero, mi preocupación surgió al hacerme la siguiente pregunta: «Los alumnos que realicen estos cursos, ¿Van a poder utilizar las herramientas las cuales están aprendiendo?». Tal vez sí, por eso forman parte de una Institución pública, libre y gratuita. Entonces, me puse a verificar sobre la posiblidad que tendríamos, una vez finalizado el curso de poder utilizar estas herramientas.

Esto es lo que encontré (Octubre 2019):

![](../uploads/2020/02/precio_ms_office.png)![](../uploads/2020/02/precio_ms_project.png)![](../uploads/2020/02/adobe.png)Lo cual me llamó demasiado la atención de que un alumno, tenga que pagar como máximo $31.000 ARS, para poder utilizar una herramienta que le enseñó una Universidad Pública. Entonces, si pensamos en un alumno, que tal vez, no tenga los recursos económicos para poder adquirir estos productos de software, para el cual dedicó tiempo (y recursos) para aprenderlos, está obligado a tomar dos caminos: 1. No utilizar la herramienta. 2. Piratear la herramienta.

El verdadero problema surje cuando en las cátedras de las diferentes carreras universitarias, ya sea del área informática, o de cualquier otra, promueven el uso del software privativo. Puede suceder, y un alumno en el encuentro me dio la razón, que algún profesor «obligue» a sus alumnos a presentar tareas, trabajos prácticas y peor aún trabajos finales de cátedra (¡ó de carrera!) en algun fórmato privativo. Llegando en ese punto a reducir los caminos que mencioné en el párrafo anterior a uno: **Piratear la herramienta**.

Dejando de lado lo ecónomico, la obligación de las Universidades es contribuir al conocimiento humano, pero como dice RMS:

*Lamentablemente, muchos administradores de universidades adoptan una actitud egoísta con respecto al software (y a la ciencia); consideran los programas como una posible fuente de ingresos, no como oportunidades para contribuir al conocimiento humano. Los desarrolladores de software libre han estado enfrentándose con esta tendencia durante casi 20 años.* \[1\]

Con las libertades que el Software Libre aporta a los alumnos (en realidad, a la sociedad en general, pero hoy me concentro en los alumnos), en este caso, los informáticos, podrán estudiar cómo funcionan los productos que normalmente se suelen utilizar, y ellos mismos podrán aportar sus habilidades al desarrollo de nuevas herramientas, o mejorar las existentes.

 Si no existe una cultura colaborativa en el aprendizaje y uso del software, no podemos pretender generar recursos humanos (informáticos) que sean capaz de no ser «egoístas», ni mucho menos futuros científicos (informáticos), dónde lo «colaborativo» es una de las primeras normas.

La presentación de esta charla se encuentra en: https://github.com/eamanu/Talks/blob/master/2019/EILAR/presentation/comunidad\_sw\_libre.tex

\[1\] https://www.gnu.org/philosophy/university.es.html