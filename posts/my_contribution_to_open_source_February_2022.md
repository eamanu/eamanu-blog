---
title: My contributions to open sources - February 2022
date: 28 Feb, 2022
author: eamanu
tags: new-blog, Debian, python, open-source
---
# My contribution to Open Source - February 2022

## Debian
This is the list of the contributions:

* sent: Update package. Minor improves. Fix `https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=970969`. Uploaded by Bart Martens `https://tracker.debian.org/news/1300596/accepted-sent-1-4-source-into-unstable/`.
* Fill [#1005181](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1005181) bug. The last upload of fsspec cause a regression in dask. This bug is reporting that.
* New upstream release, and some improve in python-ftputil like adding autopkgtest. [Uploaded](https://tracker.debian.org/news/1305585/accepted-python-ftputil-503-1-source-into-unstable/) by Jeroen Ploemen.
* Big bump new upstream release from 0.7.5 to  1.4.1. Several improves. Fix of [983655](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=983655). Waiting for sponsorship.
* citeproc-py: New upstream release 0.6.0. Waiting for sponsorship.
