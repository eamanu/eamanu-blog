---
title: My contributions to open source - October 2022
date: 31 October, 2022
author: eamanu
tags: new-blog, Debian, python, open-source
---
# My contribution to Open Source - October 2022

## Debian
This is the list of the contributions:

* mdit-py-plugins: new upstream release 0.3.1. Uploaded to [unstable](https://tracker.debian.org/news/1375034/accepted-mdit-py-plugins-031-1-source-all-into-unstable/)
* myst-parser: new upstream release 0.18.1. Uploaded to [unstable](https://tracker.debian.org/news/1377486/accepted-myst-parser-0181-2-source-into-unstable/)
* mdurl: new upstream release 0.1.2. Uploaded to unstable by [Jeroen Ploemen](https://tracker.debian.org/news/1375013/accepted-mdurl-012-1-source-into-unstable/)
* monty: New upstream release 2022.9.9. Uploaded to unstable by [Jeroen Ploemen](https://tracker.debian.org/news/1375029/accepted-monty-202299dfsg-1-source-into-unstable/)
* python-blosc: fix debian/watch file. Uploaded to [unstable](https://tracker.debian.org/news/1377487/accepted-python-blosc-1106ds1-3-source-into-unstable/)
* python-xlib: fix debian/watch file. Waiting for sponsorship.
* pythonmagick: fix debian/watch file: Uploaded to unstable by [Jeroen Ploemen](https://tracker.debian.org/news/1377810/accepted-pythonmagick-0919-9-source-into-unstable/)

# Other projects
* Create issue in monty project. Tarball for 2022.9.9 is incomplete. Tests files are missing [see](https://github.com/materialsvirtuallab/monty/issues/442).
* Create a PR in [filesystem_spec](my_contribution_to_open_source_October_2022.md) project to fix [issue 927](https://github.com/fsspec/filesystem_spec/issues/927). The issue was noted in the build of the project in Debian.
