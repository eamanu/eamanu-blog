---
title: My contributions to open source - December 2021
date: Dec 31, 2021
author: eamanu
tags: new-blog, Debian, python, open-source
---

## Debian

This is the list of the work on Debian:

  * fsspec. Working in the new upstream release 2021.11.1 <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=983883>. Still in progress. Reviewed by morph.
  * mod-wsgi. Closes <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=999857>. New upstream release 4.9.0. And several improve in the package. Uploaded by morph.
  * pastel. Fix <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1001365>. Support for run autopkgtest with python3.10. Uploaded by morph.
  * shellingham. Working in the new upstream release 1.4.0 <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1001485>. Still in progress. Reviewed by morph
  * poetry. New upstream release 1.1.12. Build it with pep517.

## Other projects

sadly No time for this :(


Happy GNU Year :-)
