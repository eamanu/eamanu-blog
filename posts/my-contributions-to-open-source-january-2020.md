---
author: eamanu
blogspot: true
date: Feb 16, 2020
tags: Old-Blog
title: "My contributions to Open Source \u2013 January 2020"

---
# My contributions to Open Source – January 2020

Good news! I applied for Debian Maintainer, the process is: https://nm.debian.org/process/710. [jcc](https://nm.debian.org/person/jcc) advocate me. 🙂

I start working on a ML + Remote sensing application, so I start contribute (just opening issues) on some GIS tools. I am planning package [sentinelsat](https://sentinelsat.readthedocs.io/en/stable/), [python-fmask](https://github.com/ubarsc/python-fmask) and [rios](https://github.com/ubarsc/rios) for Debian. Maybe starting with me own PPA.

On Debian I worked fixing issues, and removing Python 2 Support of leaf packages. On February I need to clean up my ITA/ITP packages backlog.

On CPython I send the PR: https://github.com/python/cpython/pull/18195 (still working) to raise an Deprecation Warning when a asyncio object is created with not running loop (more detail https://bugs.python.org/issue38599)

Also I send my first contribution to PyAR (Python Argentina) 🙂 The patch was a very simple solution to add the possibility of use CUIT foreign. Issue: https://github.com/PyAr/asoc\_members/issues/128. The patch: https://github.com/PyAr/asoc\_members/pull/150

I propose https://github.com/arthurdejong/python-stdnum/pull/181 for python-stdnum.Was included on this [commit](https://github.com/arthurdejong/python-stdnum/commit/0b30c4b86150e055a87dff21285a40c9d06df4ff).

I open this issue for pyar-asoc https://github.com/PyAr/pyarweb/issues/454. Also I open this issue https://github.com/pyglet/pyglet/issues/137 based on #[950039](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=950039) Debian bug.