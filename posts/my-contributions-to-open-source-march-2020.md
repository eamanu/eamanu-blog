---
author: eamanu
blogspot: true
date: Apr 18, 2020
tags: Old-Blog
title: "My contributions to Open Source \u2013 March 2020"

---
# My contributions to Open Source – March 2020

This was a Debian month. I worked on several NMU on packages (non-maintainer upload) to give support to python3.7 and python3.8. Currently Debian is trying to support two version of python3 (3.7 and 3.8). Whole packages before move from unstable to testing, need build (obviously) and run some tests (called autopkgtest). Some packages just test for one of that versions (commonly py37) and does not test for py38. That is a problem because the package can be building and installing successfully for one version, but fail for the other one. Just adding some minor changes we can test for all versions supported 😀

In the other hand, I start trying packaging poetry (https://python-poetry.org/) you can see the ITP here (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=947261). But is not so easy, there a some package that are not on Debian (like cachy: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=956607)

Also, I start on Debian Med-Team for COVID-19 Biohacking ( https://lists.debian.org/debian-devel-announce/2020/03/msg00010.html). So I started with the package of idseq-dag (https://idseq.net/) it’s on WIP (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=956456).

On the free time, I develop pys3landsat https://github.com/eamanu/pyamazonlandsat you can download using *pip install pys3landsat.* You can use this module/application to download Landsat products from S3 amazon services. For now, you can just download if you have the product. Second step is add a way to query on the products 🙂

I cannot work for any Python project 🙁