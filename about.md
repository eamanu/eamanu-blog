# About me

I'm just an open developer, and FOSS (hack¿?)activist. I'm interest on how language are
implemented and how the things works. Always looking where I can contribute and help.

Currently I'm Debian Maintainer, I actively participate on Debian Python Team, [I maintain some
packages in Debian](<https://qa.debian.org/developer.php?login=eamanu%40yaerobi.com>). 

Also, I contribute in different projects (not so active as in Debian) like python-docs-es, 
CPython, pip, pytest, etc...

My gpg key is `13796755BBC72BB8ABE2AEB5FA9DEC5DE11C63F1`.

You can found me in IRC as eamanu in OFTC servers or Libera

Also follow me:

* [Twitter](https://twitter.com/eamanu314)
* [Gitlab](https://gitlab.com/eamanu)
* [Salsa (Debian)](https://gitlab.com/eamanu)
* [Github](https://github.com/eamanu)

Just write me: eamanu@members.fsf.org
