---
author: eamanu
blogspot: true
date: Dec 01, 2018
tags: Old-Blog
title: "My contribution to open source \u2013 November 2018"

---
# My contribution to open source – November 2018

This month I to the next contributions:

- I do 36 review from two repository: scikit-learn and cpython the list of this code review (given by Github) is:

![](../uploads/2018/12/image-1.png)- I create two PR to scikit-learn:
  - [Fix Support Haversine distance in NearestNeighbors issue](https://github.com/scikit-learn/scikit-learn/pull/12568) That fix [\#12552](https://github.com/scikit-learn/scikit-learn/issues/12552). Related to [\#4453](https://github.com/scikit-learn/scikit-learn/issues/4453)[  ](https://github.com/scikit-learn/scikit-learn/pull/12568)
  - [pairwise\_distances(X) should always have 0 diagonal intial commit ](https://github.com/scikit-learn/scikit-learn/pull/12635)That fix [\#12628](https://github.com/scikit-learn/scikit-learn/issues/12628)
- In the other hand I had been working in an algorithm named Rank Based Outlier Detection to implement it on scikit-learn-contrib. The idea and the algorithm implements itself is was made by Jakob Zeitler &lt;mail@jakob-zeitler.de &gt;

Debian




I don’t have so much contribution to Debian Project. Just this:

- Upload to mentors.d.o python-pcapy0.11.4-1
- Upload to mentors.d.o python-cassandra-driver/3.16.0-1
- Upload to mentors.d.o impacket 0.9.17-1
- Upload to mentors.d.o pygithub 1.43.3-1
- Upload to mentors.d.o python-scp 0.13.0-1