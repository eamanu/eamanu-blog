---
title: My contributions to open source - August 2022
date: 31 August, 2022
author: eamanu
tags: new-blog, Debian, python, open-source
---
# My contribution to Open Source - August 2022

## Debian
This is the list of the contributions:

* poetry-core: Working in package of beta version: 1.1.0b3. Waiting for sponsorhip.
* python-flask-jwt-extended: New upstream version 4.4.3. Apply patch to use TestReponse.data instead of TestResponse.text. In current version of python-werkzeug this property is not added.
* monty: Uploaded to unstable by [Stefano Rivera](https://tracker.debian.org/news/1259676/accepted-monty-2021817dfsg-1-source-into-unstable/). Also monty 2022.4.26+dfsg-1 add documentation package, so this need to go to [NEW](https://ftp-master.debian.org/new/monty_2022.4.26+dfsg-1.html) 
* poetry: New upstream release 1.1.14. Uploaded by [Louis-Philippe Véronneau](https://tracker.debian.org/news/1356321/accepted-poetry-1114dfsg-1-source-into-unstable).
* python-xlib: Working in a new usptream release 0.31. Packaging of the documentation. Uploaded to experimental by [Stefano Rivero](https://packages.debian.org/source/experimental/python-xlib). Open issue https://github.com/python-xlib/python-xlib/issues/231 about a tests that is falling in Debian builds.
* jupyter-packaging: New upstream release 0.12.3. Uploaded to [unstable](https://tracker.debian.org/news/1358158/accepted-jupyter-packaging-0123-1-source-all-into-unstable/).
* python-flask-jwt-extended: New upstream 4.4.4. Uploaded to [unstable](https://tracker.debian.org/news/1358366/accepted-python-flask-jwt-extended-444-1-source-all-into-unstable/).

# Other projects
* Open issue [flask-jwt-extended](https://github.com/vimalloc/flask-jwt-extended/issues/490)
* Open issue [python-xlib](https://github.com/python-xlib/python-xlib/issues/231)

