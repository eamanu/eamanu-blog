---
title: DebConf20
date: Aug 30, 2020
author: eamanu
tag: Old-Blog
---

# DebConf20

DebConf20 finished. This was my second DebConf in my life. Once again after the DebConf I am motivating to continue contributing on the Debian and be part of this big community.

In this year my aim is try to crate a local team on my city to promove the use and contribution on Debian.

One of the interest talk, that I would like to follow is the use of the Debian on Raspbery. Interest links: <https://salsa.debian.org/raspi-team/web-raspi-img> and <https://salsa.debian.org/raspi-team/image-specs>.

![](../uploads/2020/08/180.png)One think! This’s was my first time that I present a Talk on DebConf \\o/. <https://debconf20.debconf.org/talks/92-mi-experiencia-creando-una-comunidad-local-en-una-ciudad-pequena/> and <https://debconf20.debconf.org/talks/55-an-experience-creating-a-local-community-in-a-small-town/>

Archive of DebConf20 <https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/>
