---
title: My contributions to Free Software - September 2023
date: 30 September, 2023
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - September 2023

## Debian
This is the list of the contributions:
* poetry-core: new usptream release 1.7.0.
* poetry: new upstream release 1.6.1. Also fix RC bug [1050688](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1050688), those tests started to fails because dh-python, now remove egg-info folders from source and, poetry has some of thise folder for tests purpose.
* umask: New upstream release 2.1.0.
* aiodogstatsd: New package version: 0.16.0-3. Minor update in package.
* monty: New upstream release 2023.9.5.

