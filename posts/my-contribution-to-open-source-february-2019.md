---
author: eamanu
blogspot: true
date: Mar 24, 2019
tags: Old-Blog
title: "My contribution to open source \u2013 February 2019"

---
# My contribution to open source – February 2019

At March 24th I will tell my contributions to open source at February.

![](../uploads/2019/03/image.png)well,

- I created a new repo: https://github.com/eamanu/Mozio\_test because I was searching a new job, so I made this to send to Mozio. (I not finished the exercise by issue time) If any want to take it for something, is free. 🙂
- I send two docs commit to cpython :
  - https://github.com/python/cpython/commit/df5cdc11123a35065bbf1636251447d0bfe789a5
  - https://github.com/python/cpython/commit/522630a7462f606300f1e6e6818de191d9dc3fdf
- I send in total 10 PR:
  - 3 PR (2 opened and 1 closed) on https://github.com/zooba/reportabug
  - 4 PR to cpython (2 merged and 2 still open)
  - 1 PR to cpython (still open)
  - 1 PR to Snek (closed)
  - 1 PR on flask (still open)
- I made 55 Code Review, 54 on cpython and 1 on cpython/devguide
- I open 4 Issues on:
  - 2 on snek (closed both)
  - 1 on twitter-scrape
  - 1 on reportabug