---
title: My contributions to Free Software - June 2023
date: 30 June, 2023
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - June 2023

## Debian
This is the list of the contributions:

* monty: New upstream release 2023.5.8. Waiting for sponsorship.
* palletable: Prepare upload to unstable of version 3.3.3. Uploaded to [unstable](https://tracker.debian.org/news/1435310/accepted-palettable-333-1-source-into-unstable/).
* flask-jwt-extended: New upstream release 4.5.2. Uploaded to [unstable](https://tracker.debian.org/news/1435413/accepted-python-flask-jwt-extended-452-1-source-into-unstable/).
* markdown-it-py: New upstream release 3.0.0. Uploaded to unstable.
* mdit-py-plugins: New upstream release 0.4.0. Uploaded to unstable.
* monty: New upstream release 2023.5.8. Uploaded to unstable.
* shellingham: Preparing version 1.5.1 to upload to unstable.
* myst-parser: New upstream release 2.0.0. Uploaded to unstable.

# Other projects

* git-flight-rules: Clarify the use of --amend (see [#353](https://github.com/k88hudson/git-flight-rules/issues/353)). Fix [here](https://github.com/k88hudson/git-flight-rules/pull/354).
* mdit-py-plugins: Report minor issue in pyproject.toml file to avoid confusion about the markdown-it-py version in dependencie. See [#92](https://github.com/executablebooks/mdit-py-plugins/issues/92).
