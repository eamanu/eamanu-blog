---
author: eamanu
blogspot: true
date: Apr 03, 2020
tags: Old-Blog
title: 'Zoom: Problema al compartir pantalla en Debian Testing'

---
# Zoom: Problema al compartir pantalla en Debian Testing

Una entrada muy corta, por si alguien tiene problemas al compartir la pantalla usando Debian Testing (tengo la sospecha que lo mismo sucede en Debian 10, pero no lo probé ni tampoco investigué).

Con el tema de la cuarentena, muchas entidades, empresas, universidades, clases particulares y demás comenzaron a utilizar esta herramienta. En mi caso comencé a dar un curso de Python para la Universidad, y lo hacemos vía Zoom. Cuando estuve realizando pruebas (días previos) para ver cómo funcionaba la herramienta, al momento de querer compartir la pantalla tenía un error similar al siguiente: *can not start share, wayland has not been supported yet, please use x11 instead*.

Wayland es un protocol de comunicación de tipo cliente servidor para comunicarse con las pantallas (displays), en fin, no viene al caso.

Entonces, cómo se soluciona este problema? o mejor dicho voy a tener que reinstalar mi sistema operativo para llevarlo a otro que sea compatible? La respuesta es **no** :). Esto se soluciona simplemente modificando el archivo /etc/gdm3/daemon.conf ( en algunos se llama custom.conf) y buscar la linea:

\# WaylandEnable=false

Ahora la magia descomentar esa linea (borrando el #) y reinciar la máquina y listo. Ahora podes compartir pantalla en zoom

Nota aparte:

Debido a mis intenciones de promover el uso software de código abierto y libre, debo recomendar el **NO USO DE ZOOM**, esto se debe a que es una herramienta privativa al cual no tenemos acceso a su código y por lo tanto está dañanado nuestras libertades escenciales, como así también puede estar realizando cualquier actividad por detrás que no tenemos conocimiento. asi que recomiendo fuertemente, convencer a los empleadores y quienes usen esta herramienta, de dejar de hacerlo y utilizar herramientas libres por ejemplo <https://jitsi.org/>

Dejo además problemas de seguridad y recomendaciones de no utilizar Zoom <https://securityboulevard.com/2020/03/using-zoom-here-are-the-privacy-issues-you-need-to-be-aware-of/>

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter"><div class="wp-block-embed__wrapper">> Sobre Zoom:  
>   
> 1\) No lo usen.  
>   
> 2\) Si no les queda otra que usarlo:  
>   
>  2.1) No se registren con su cuenta de Facebook.  
>   
>  2.2) No corran ningún programa ni hagan nada más en la computadora o el celular mientras lo usen.  
>   
>  2.3) Pidan a quien los obliga que busque una alternativa.
> 
> — Javier Smaldone (@mis2centavos) [March 27, 2020](https://twitter.com/mis2centavos/status/1243397704929038343?ref_src=twsrc%5Etfw)

<script async="" charset="utf-8" src="https://platform.twitter.com/widgets.js"></script></div></figure>Nos vemos!