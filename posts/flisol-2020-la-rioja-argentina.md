---
author: eamanu
blogspot: true
date: Jun 11, 2020
tags: Old-Blog
title: FLISOL 2020 La Rioja, Argentina.

---
# FLISOL 2020 La Rioja, Argentina.

<figure class="wp-block-image">![No hay texto alternativo para esta imagen](https://media-exp1.licdn.com/dms/image/C4D22AQFEfLfieWOJ5A/feedshare-shrink_1280/0?e=1594857600&v=beta&t=jG48XbCfHDJf7F5AawuQq3U2LjIJ5xfM36tHP_nnwOs)</figure>El pasado 30 de junio se llevó a cabo con éxito el FLISOL (Festival Latinoamericano de Instalación de Software Libre) 2020 edición La Rioja <https://flisol.info/FLISOL2020/Argentina/LaRioja>, que esta vez se realizó de manera virtual por el problema globalmente conocido de la pandemia del Covid-19. Debido a esta situación, además, el evento en La Rioja fue en una fecha diferente a la que se desarrolla normalmente, el 4to sábado del mes de abril de cada año.

Antes de continuar, debo agradecer a la organización de la FLISOL: José Andrés Tejerina, Pablo Corzo, Mercedes Lobos, Mauricio Lahitte y especialmente a Nicolas y Marcos que estuvieron en el día del evento, gestionando el horario de los speakers y haciendo streaming a Youtube.

Esta FLISOL fue planeada originalmente para ser realizada en la Universidad Nacional de La Rioja, pero que por fuerza mayor (ya mencioandas anteriormente) no se pudo realizar, pero aún así la UNLaR siguió participando activamente en la organización y difusión del evento. Por ello quiero agradecer al Decano Mg. Marcelo Martinez, al Lic. Juan Pablo Millicay y a todo el Departamento Académico de Ciencias Exáctas Físicas y Naturales (DACEFyN) y a la Secretaría de Graduados.

Por último queda agradecer a la parte escencial del evento, a los speakers (ordenados por el schedule del evento):

- Marcos Peña Pollastri
- Hugo Chanampe
- Leonardo Garay
- Juan Pablo Millicay
- Cristian Carrizo
- Emiliano Lopez
- Horacio Mayo
- Miguel Barraza
- Juan Pedro Fisanotti

Pueden consultar el schedule del evento en: <https://eventol.flisol.org.ar/events/flisol-larioja-2020/schedule> y pueden revivirlo en: https://youtu.be/wPN\_Jx4jTO8

<figure class="wp-block-image">![No hay descripción alternativa para esta imagen](https://media-exp1.licdn.com/dms/image/C4E22AQEkBnUZ2y-PPQ/feedshare-shrink_800/0?e=1594857600&v=beta&t=WKWMo3GETroRxHnPkrAnudNTDJVWR4i6AxQWaaNhMO8)<figcaption>Apertura FLISOL 2020 La RIoja</figcaption></figure><figure class="wp-block-image">![Imagen](https://pbs.twimg.com/media/EZSVHZPWsAUT-gg?format=jpg&name=large)<figcaption>Marcos Peña Pollastri</figcaption></figure><figure class="wp-block-image">![Imagen](https://pbs.twimg.com/media/EZSwK8EWoAEAjSs?format=jpg&name=small)<figcaption>Miguel Barraza</figcaption></figure><figure class="wp-block-image">![Imagen](https://pbs.twimg.com/media/EZSpN6dXYAcGX0l?format=jpg&name=small)<figcaption>Horacio Mayo</figcaption></figure><figure class="wp-block-image">![Imagen](https://pbs.twimg.com/media/EZShyMEWAAEBOz7?format=png&name=small)<figcaption>Emiliano Lopez</figcaption></figure><figure class="wp-block-image">![Imagen](https://pbs.twimg.com/media/EZSa4UaWoAEDVAa?format=jpg&name=small)<figcaption>Cristian Carrizo</figcaption></figure><figure class="wp-block-image">![Imagen](https://pbs.twimg.com/media/EZSR4w5X0AA-Qp1?format=jpg&name=small)<figcaption>Juan Pablo Millicay</figcaption></figure>