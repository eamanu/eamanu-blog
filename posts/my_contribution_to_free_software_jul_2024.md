---
title: My contributions to Free Software - July 2024
date: 31 Jul, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - July 2024

## Debian
* python-cloudflare: Add patch to fix Syntax Warning in version 2.11.1-2 to fix RC bug [#1061824](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1061824).
* python-ulmo: Fix RC Bug [#1074525](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1074525). Replace StrictVersion from deprecated distutils to Version from packaging.
* docker-compose: Fix RC bug [#1065848](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065848). This remove python3-distutils from the package.
* doxyqml: Fix RC bug [#1065848](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065848). This remove python3-distutils from the package.
* capuccino: Fix RC bug [#1065837](https://bugs.debian.org/1065837). This remove python3-distutils from the package.
* docker-pycreds: Fix RC bug [#1065849](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065849). This remove python3-distutils from the package.
* grokevt: Fix RC bug [#1065871](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065871). This remove python3-distutils from the package.
* fonts-cantarell: Fix RC bug [#1065860](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065860). This remove python3-distutils from the package.
* matrix-synapse: Fix RC bug [#1065900](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065900). This remove python3-distutils from the package.
* poetry: Fix FTBFS [#1074725](https://bugs.debian.org/1074725). Also remove python3.11-venv package from Build Dependency.
* mm-common: Fix RC bug [#1065903](https://bugs.debian.org/1065903). This remove python3-distutils from the package.
* docker-compose: Fix bug [#1076201](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1076201) that I introduced during [#1065848](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065848) fixing.
* openscap-daemon: Fix RC bug [#1065912](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065912). This remove python3-distutils from the package.
* tulip: Fix RC Bug [#1065989](https://bugs.debian.org/1065989). This remove python3-distutils from the package.
* wfuzz: Apply ginngs's patch  to fix two RC bugs [#1066009](https://bugs.debian.org/1066009) anbd [#1067598](https://bugs.debian.org/1067598). This remove python3-distutils from the package.
* scikit-build: New upstream release 0.18.0. Fix RC bug [#1076313](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1076313) applying patch attached by Vladimir Petko.

## DebConf24
* fsspec: New upstream release 2024.6.1.
* Sending [my first patch](https://lore.kernel.org/linux-staging/20240730070059.33210-1-eamanu@riseup.net/#related) and accepted in Kernel Linux.

### Sponsored Package
* pyro5: New upstream release 5.15. Kudos to Bo YU <tsu.yubo@gmail.com>.
* shotwell: New upstream release 0.32.7. Kudos to Jörg Frings-Fürst <debian@jff.email>.
* python-asyncclick: New upstream release 8.1.7.2. Kudos to Carles Pina i Estany <carles@pina.cat>.
* python-pyaarlo: New upstream release 0.8.0.3. Kudos to Carles Pina i Estany <carles@pina.cat>.
* python-certbot: Fix RC bug [#1065923](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065923). This remove python3-distutils from the package. Kudos to  James Valleroy <jvalleroy@mailbox.org>.

## Other projects
* ulmo: Create a PR to replace StrictVersion from deprecated distutils to Version from packaging. See [PR #222](https://github.com/ulmo-dev/ulmo/pull/222).
* grokevt: Create a PR to remove distutil from the package. See [PR #1](https://github.com/ecbftw/grokevt/pull/1)

