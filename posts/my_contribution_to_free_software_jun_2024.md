---
title: My contributions to Free Software - June 2024
date: 30 Jun, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - June 2024

## Debian
* python-release: New upstream release. Fix two bugs [#1067826](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1067826) and [#1073488](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1073488).
* fsspec: New upstream release 2024.6.0.
* unifrac: Fix bug due the python-skbio new upstream version. See [#1074302](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1074302).

### Sponsored Package
* cozy: New upstream release 1.3.0. Kudos to Manuel Traut <manut@mecka.net>.
* python-pdoc: New upstream release 14.5.0. Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.
* python-scpi: Introduce version 2.4.0 to Debian. Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.
* python-graphene-directives: Introduce version 0.4.6 to Debian. Kudos to Ananthu C V <weepingclown@disroot.org>.
* python-graphene-federation: Introduce version 3.2.0 to Debian. Kudos to Ananthu C V <weepingclown@disroot.org>.
* accessible-pygments: Introduce version 0.0.5 to Debian. Kudos to Bo YU <tsu.yubo@gmail.com>.
* haproxy-log-analysis: New upstream release 6.0.0a1. Kudos to  Christopher Baines <mail@cbaines.net>.

## Other projects
* unifrac: Send patch from Debian to fix tests and for work with scikit-bio >= 0.6.0. See [PR#160](https://github.com/biocore/unifrac/pull/160).
