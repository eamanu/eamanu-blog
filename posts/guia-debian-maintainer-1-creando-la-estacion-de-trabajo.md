---
author: eamanu
blogspot: true
date: Sep 03, 2018
tags: Old-Blog
title: "Gu\xEDa Debian Maintainer [1/?] &#8211; Creando la estaci\xF3n de trabajo."

---
# Guía Debian Maintainer [1/?] &#8211; Creando la estación de trabajo.

Buenas, empiezo esta serie de entradas con el mismo motivo que lo hago en todas mis entradas: llevar un registro de las cosas que hago, o de los problemas que voy teniendo, y la manera en que puedo resolverlo. ¿Para qué? Simplemente porque tengo una memoria poco confiable y además, si alguien tiene ese mismo problema, contribuir un poquito en ayudarle. Así que escribo como si alguien más lo estuviera leyendo (Si alguien más está leyendo esto, gracias).

Hace ya un tiempo, comencé a contribuir en el proyecto Debian. Y como no soy muy bueno aprendiendo cosas nuevas, todo esto me cuesta. Así que de a poquito, y con muchas horas de lectura puedo ir sacando cosas en concluso. Así que el propósito de esta serie de entradas (ojala que sea más de una) es los pasos para comenzar a contribuir en el proyecto Debian.

Antes que nada, por si no lo haz hecho, te dejo el link a la documetación de Debian: https://www.debian.org/doc/ Sería bueno que también empieces leyendo la [guía de nuevos mantenedores](https://www.debian.org/doc/devel-manuals#maint-guide) (existe esa palabra?) de Debian.

Lo primero que recomiendo, por supuesto, algo que NO hice, pero ahora lo haré, es trabajar en una máquina virtual. Por que? Porque puede ser que necesites probar, paquetes que no son estables y que te ayudarían, en el peor de los casos, a tener la necesidad de instalar de nuevo el S.O. (si es que no sos todavía muy conocedor de Debian, como yo). Para comenzar necesitarías descargarte una [ISO de Debian.](https://www.debian.org/CD/) Pero ya que vamos a utilizarlo, no como sistema estable, si no de pruebas, por ahí te conviene instalar sus versiones de «testing»: https://www.debian.org/devel/debian-installer/. Si entras, podrás ver que hay snapshot sacadas semanalmente y snpashots sacadas diariamente.

Una vez que las descargues necesitaras una VM. No voy a mostrar aquí cómo instalar Debian con una máquina virtual. En todo caso dejo links de interés:

- https://www.hiroom2.com/2017/06/26/debian-9-install-kvm/
- https://wiki.debian.org/KVM
- https://www.brianlinkletter.com/installing-debian-linux-in-a-virtualbox-virtual-machine/
- https://wiki.debian.org/VirtualBox
- https://www.linuxbabe.com/debian/install-virtualbox-debian-9-stretch

En particular yo uso qemu/kvm y virtual manager.

<figure class="wp-block-image">![](../uploads/2018/09/image.png)<figcaption>VM con Debian para testing</figcaption></figure>Para que me sea más cómodo le instalé Openssh server para entrar para no tener que usar directamente la VM.

### Programas necesarios

Siguiendo el manual del nuevo desarrollador, necesitamos una serie de programas básicos para comenzar a empaquetar. El primero que menciona es el *build-essential.* Este paquete es básico. Entonces procedemos a instalarlo, haciendo : *sudo apt install build-essential -y*

![](../uploads/2018/09/image-1.png)Listo. Luego, se necesitan otros programas, que si bien no son necesarios, algunos paquetes podrían necesitar tenerlos instalados. Los cuales son los siguientes:

- autconf, automake, autools-dev
- dh-make y debhelper
- devscript
- fakeroot
- file
- gfortran
- git
- gnupg
- gpc
- lintian
- patch
- patchutils
- pbuilder
- perl
- python
- quilt
- xutils-dev

El comando que utilizo para instalar todos los paquetes es el siguiente:

<script src="https://gist.github.com/eamanu/953692caa7dfa15eb4e6bd9f2492e8ad.js"></script>Esto tomará un tiempo.

![](../uploads/2018/09/image-2.png)Una vez que esté instalado todos los programas necesarios, ya podemos comenzar a empaquetar.

Pero por ahora, es suficiente. Nos vemos!