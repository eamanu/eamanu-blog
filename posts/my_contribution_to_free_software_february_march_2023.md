---
title: My contributions to Free Software - February & March 2023
date: 31 March, 2023
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - February & March2023

This was a weird begin of the year. Sad situations occupied my days, but here we go. I hope start increase my contributions in the next months :-)

## Debian
This is the list of the contributions:

* cookiecutter: Fix RC bug #1033431. Waiting for sponsorship.
* markdown-it: Fix security bug #1031764. Waiting for sponsorship.


# Other projects
