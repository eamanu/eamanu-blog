---
title: My contributions to Free Software - December 2023
date: 31 December, 2023
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - December 2023

## Debian
* poetry-plugin-export: Introduce version 1.6.0 to Debian.
* scikit-build-core: New upstream release 0.7.0. Also, fix package to build for Python3.12 [see here](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1056528).
* monty: Fix autopkgtest tests [#1056427](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1056427).
* flask-jwt-extended: New upstream release 4.6.0.

### Sponsored Package
* python-pyrgg: Introduce version 1.4 to Debian. Kudos Yogeswaran Umasankar <kd8mbd@gmail.com>.
* asyncclick: Introduce version 8.1.7.0+async-1 to Debian. Kudos Carles Pina i Estany <carles@pina.cat>.
* python-pycm: Introduce version 4.0 to Debian. Kudos Kudos Yogeswaran Umasankar <kd8mbd@gmail.com>.
* python-cloudscraper: New upstream version: 1.2.71~git20230426.cbb3c0ea-1. Kudos Carles Pina i Estany <carles@pina.cat>.
* python-questionary: Introduce version: 2.0.1-1. Kudos Ananthu C V <weepingclown@disroot.org>
* python-opem: Introduce version: 1.3+dfsg-1. Kudos Yogeswaran Umasankar <kd8mbd@gmail.com>.
* python-disptrans: Uploaded version 0.0.1-1 to fix bug [#1058480](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1058480). Kudos Yogeswaran Umasankar <kd8mbd@gmail.com>.

## Other projects
* Nothing this month :(
