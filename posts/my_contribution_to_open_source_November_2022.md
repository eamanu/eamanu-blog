---
title: My contributions to open source - November 2022
date: 30 November, 2022
author: eamanu
tags: new-blog, Debian, python, open-source
---
# My contribution to Open Source - November 2022

## Debian
This is the list of the contributions:

* fsspec: 
  - Make the package reproducible. Use LC\_ALL and LANGUAGE variables to fix the unreproducible issue.
  - New upstream release.
  - Upload to [unstable](https://tracker.debian.org/news/1384007/accepted-fsspec-2022110-1-source-into-unstable/)
* jcc: New upstrem release with support for python3.11. Closing [#1023906](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1023906). Uploaded to [unstable]()
* poetry-core: New upstream release 1.3.2. Uploaded to [unstable](https://tracker.debian.org/news/1385473/accepted-poetry-core-132-1-source-into-unstable/)
* cleo: Upload to 1.0.0a5 to be compatible with the new version of poetry. Uploaded into [unstable](https://tracker.debian.org/news/1389359/accepted-python-cleo-100a5-1-source-into-unstable/)
* poetry: New upstream release: 1.2.2. Still working on.

# Other projects
* DataStax Python Driver: Report possible flaky test test\_nts\_token\_performance. The reported issue is [PYTHON-1315](https://datastax-oss.atlassian.net/browse/PYTHON-1315)
* DataStax Python Driver: Report [error](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1024043) during the build using Python3.11. The reported issue is  [PYTHON-1316](https://datastax-oss.atlassian.net/browse/PYTHON-1316)
* python-cleo: Report [issue](https://github.com/python-poetry/cleo/issues/284) CVE-2022-42966: exponential ReDoS. Originally reported in [bugs.debian.org](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1024018).
