---
title: My contributions to Free Software - August 2023
date: 31 August, 2023
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - August 2023

## Debian
This is the list of the contributions:
* poetry-core: New upstrem release 1.6.1. Uploaded to unstable.
* poetry: New upstream release 1.5.1. Uploaded to unstable.
* python-scp: Version 0.14.5-2. Remove autopkgtest folder that I packaged wrongly.
* monty: monty 2023.5.8+dfsg-5. Remove python3-torch from Build Depends to avoid build in certain architectures.
* scikit-build: New upstream release 0.17.6. Fix [#1042353](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1042353).
* python-requests-toolbelt: New upstream release 1.0.0.
* jupyter-packaging: Fix FTBFS [#1042290](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1042290)
* poetry: Fix [#1050362](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1050362). This was an issue in the version of python3-poetry-core dependency.
* shellingham: New upstream release 1.5.3.

### Sponsored uploads

* pycryptodome: New upstream release 3.18.0. Thanks Ileana Dumitrescu for your contributions to Debian.
* ubelt: New upstream release 1.3.3. Thanks  Bo YU for your contributions to Debian.
* apispec: New upstream release 6.3.0. Thanks Ileana Dumitrescu for your contributions to Debian.
* mathlibtools: New upstream release 1.3.2. Thanks Ileana Dumitrescu for your contributions to Debian.

# Other projects
* shellingham: Propose patch to fix a failed test. See [here](https://github.com/sarugaku/shellingham/pull/82).
