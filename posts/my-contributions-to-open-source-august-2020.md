---
author: eamanu
blogspot: true
date: Sep 21, 2020
tags: Old-Blog
title: "My contributions to Open Source \u2013 August 2020"

---
# My contributions to Open Source – August 2020

August was an amazing month. I participate on the ontline DebCon20 and I make my first talk on a DebConf (<https://eamanu.com/blog/debconf20/>). From there, a lot of ideas came to light. For example, create a local groups on La Rioja and now officially the Debian Academy <https://wiki.debian.org/DebianAcademy> I am preparing some packaging tutorial.

In this month I work hard to try to release mod-wsgi <https://tracker.debian.org/pkg/mod-wsgi>, done :). Also I’m prepare some package update for libapache2-mod-python, but is waiting for review <https://salsa.debian.org/python-team/packages/libapache2-mod-python>.

New upstream release for pdfminer and python3-monty go to NEW.

In the other hand I was working on python-docs-es (Python Documentation translation to Spanish, join us ;)). Also I was my first commits on pip. And make some reviews on CPython.