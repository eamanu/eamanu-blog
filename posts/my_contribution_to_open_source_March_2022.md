---
title: My contributions to open source - March 2022
date: 31 Mar, 2022
author: eamanu
tags: new-blog, Debian, python, open-source
---
# My contribution to Open Source - March 2022

## Debian
This is the list of the contributions:

* citeproc-py: New upstream release 0.6.0. Uploaded by [Jeroen Ploemen](https://tracker.debian.org/news/1309674/accepted-citeproc-py-060-2-source-into-unstable/).
* fsspec: New upstream release 2022.02.0. Uploaded by[Jeroen Ploemen](https://tracker.debian.org/news/1309674/accepted-citeproc-py-060-2-source-into-unstable/).
* scalene: several improve. Fix of bugs [983655](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=983655). Uploaded by [Nilesh Patra](https://tracker.debian.org/news/1310023/accepted-scalene-141-1-source-into-unstable/).
* aiodogstatsd: New upstream release 0.16.0 and severals improve in the Debian package. Uploaded by [Nilesh Patra](https://tracker.debian.org/news/1312860/aiodogstatsd-0160-2-migrated-to-testing/)
* mdurl: Debianize of mdurl. ITP: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1008619.
* markdown-it-py: Work in new upstream release 2.0.1. mdurl package is need in Debian before upload.

## Other projects
* filesystem\_spec: [issue 927](https://github.com/fsspec/filesystem_spec/issues/927) reporting an issue regarding to some tests that failed in [Debian build](https://buildd.debian.org/status/fetch.php?pkg=fsspec&arch=all&ver=2022.02.0-1&stamp=1647091024&raw=0).
Possible issue is because the tests run in a host with no-loopback v6.

