---
author: eamanu
blogspot: true
date: Oct 19, 2019
tags: Old-Blog
title: My contributions to Open Source &#8211;  July/September 2019

---
# My contributions to Open Source &#8211;  July/September 2019

3 months pass from my last entry about my contributions to Open Source.

Several things happened in these months, so I will try to resume it. The best things was that I could participate of the DebConf 2019 on Curitiba Brasil. Thanks for Debian organization and SPI I could travel to Brazil to participate on my first DebConf. There, I can knew a lot of people that I know just from email or blogs. I participate on the Geographical Diversity BoF (https://debconf19.debconf.org/talks/138-geographical-diversity-bof/) where we talk about exist very few amount of open developers on southern hemisphere.

<figure class="wp-block-image">![](https://wiki.debian.org/DebConf/19/Photos?action=AttachFile&do=get&target=debconf19_group.jpg)</figure>On July I participate on the «Feria del Libro» in my city La Rioja (Argentina). I talk about «Software Libre: El qué, el cómo y el por qué» (Free Software: what, how and why). My intention was introduce to people about danger of the use of Private Software, that was a talk to not IT people.

<figure class="wp-block-image">![](https://media.licdn.com/dms/image/C4D22AQEcI7mhSJTlgg/feedshare-shrink_8192/0?e=1574294400&v=beta&t=NqWZSnUatjvcDMzT-geINcd5sdAU9hP2c2_dmKaUS4M)</figure>I also participate on the «Technological Entrepreneur Day» on La Rioja (Argentina) and I talk about the possibility of create a business/startup using that produce/use free an open source.

Also, I am happy because I could create on September the Free and Open Source Community on La Rioja Argentina with the purpose of start contribute easily on free an open source projects (this derives0 of the discussion on Geographical Diversity BoF on DebConf2019)

<figure class="wp-block-embed-wordpress wp-block-embed is-type-wp-embed is-provider-subsecretaria-de-graduados"><div class="wp-block-embed__wrapper">https://graduados.unlar.edu.ar/en-la-unlar-se-realizara-la-primera-reunion-de-la-comunidad-local-de-software-libre-y-codigo-abierto/ </div></figure><figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio"><div class="wp-block-embed__wrapper"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="608" src="https://www.youtube.com/embed/QWW2bFDskhs?feature=oembed" title="U Noticias- Buscan impulsar comunidad de software libre en la UNLaR" width="1080"></iframe></div></figure>I decided work to be a Debian Developer and a Python Core Dev so, I am planning write a series of entry about that.

This is all for today. Good bye 🙂