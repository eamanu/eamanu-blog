---
title: Poetry package in Debian
date: 7  January, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# Poetry package in Debian [1]

I maintain poetry package in Debian and I would like to write some blog entries, basically for two reasons: 1. I want to just write a blog, and 2. because I think that I can improve more things in poetry package if I write about it. I do not pretend write about the poetry itself, I want to talk about the Debian package.

I wan to be clear, the poetry package in Debian is under Debian Python Team umbrella, so anyone (in the team) can improve it (or fix something), or if you are not in the DPT, you can also send some patches or Merge Requests 🙂. But all the packages need an human maintainer so here I am.

If you do not like something about this entry, or you have some comments, just write me.

The `poetry` is a popular tool for managing Python dependencies and packaging Python projects. It provides a convenient way to create, manage, and publish Python packages. With `poetry`, developers can easily define project dependencies, manage virtual environments, and build distributable packages. It also offers features such as dependency resolution, version management, and dependency locking.

We can mention two important package here. First we have the [poetry-core package](https://salsa.debian.org/python-team/packages/poetry-core) and the [poetry package](https://salsa.debian.org/python-team/packages/poetry).

Poetry-core is a pep517 build backend that make the magic. It is transparent for everyone is used by poetry. When you install poetry, poetry-core is installed. If you are building a Debian package that is built with Poetry, you need to add as Build Dependency the python3-poetry-core package, not poetry (also do not forget add pybuild-plugin-pyproject package that is needed as well).

As a description about how the package is build up-to-date we can mention that poetry-core needs for build python3-all, python3-all-dev, python3-core-dev (yes it need itself to build), python3-setuptools and python3-virtualenv.

The binary python3-poetry-core breaks the next versions of packages python3-poetry (<< 1.7.0+dfsg-1) and python3-cleo (<< 2.0.1-1), so we need to be careful when we update those packages.

poetry-core Debian package has not several patches, just one. And yes is for deactivate some tests that fail during the build. This is my big tech debt. In poetry-core I do not have to much tests deactivated, but in poetry… Let’s talk about poetry in a next entry.

The tests that I skipped and [need more investigation](https://salsa.debian.org/python-team/packages/poetry-core/-/blob/debian/master/debian/patches/0001-Skip-test-that-fail-on-the-build.patch?ref_type=heads) are:

- test_wheel_c_extension
- test_default_with_excluded_data
- test_default_src_with_excluded_data

And in [debian/rules](https://salsa.debian.org/python-team/packages/poetry-core/-/blob/debian/master/debian/rules?ref_type=heads) I skipped the test_version_is_synced test because it read the self pyproject.toml file and during the build this file is not available. But note that in autopkgtest test I copy the file and run the [tests](https://salsa.debian.org/python-team/packages/poetry-core/-/blob/debian/master/debian/tests/pytest?ref_type=heads).

Note that I removed files like LICENSE, vendor.txt and COPYRIGHT because they are installed in the `site-packages` folder, but they should be go to documentation folder, I guess. Similar confusion was in this [issue](https://github.com/tchellomello/python-ring-doorbell/issues/324), but this a decision from Poetry project. For now we have to remove it or move it.

As you can see poetry-core is a simple package without secrets, and I think that is a good start at least for me and for my old blog.

I am interesting in to know how is package it in others Linux distributions.

All comments are welcome to eamanu@debian.org 🙂

See you soon!

