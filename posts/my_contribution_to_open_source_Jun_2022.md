---
title: My contributions to open source - June 2022
date: 30 June, 2022
author: eamanu
tags: new-blog, Debian, python, open-source
---
# My contribution to Open Source - June 2022

## Debian
This is the list of the contributions:

* python-cassandra-dirver: New upstream release 3.25.0. Uploaded to unstable by [bage@debian.org](https://tracker.debian.org/news/1330763/accepted-python-cassandra-driver-3250-1-source-into-unstable/)
* myst-parser. New upstream release 0.17.2. Uploaded by [bage@debian.org](https://tracker.debian.org/news/1331022/accepted-myst-parser-0172-1-source-into-unstable/)
* python-mitogen: New upstream release 0.3.2. Uploaded by [bage@debian.org](https://tracker.debian.org/news/1331019/accepted-python-mitogen-032-1-source-into-unstable/)
* python3-flask-jwt-extended: New usptream release 4.4.1. Uploaded to [unstable](https://tracker.debian.org/news/1331710/accepted-python-flask-jwt-extended-441-1-source-all-into-unstable/)
* jupyter-packaging: New upstream release 0.12.1. Uploaded to [unstable](https://tracker.debian.org/news/1333418/accepted-jupyter-packaging-0121-1-source-all-into-unstable/)
* scalene: New upstream release 1.5.8. Waiting for review.
* myst-parser: New usptream release 0.18.0. Uploaded to unstable.
* mdurl: Package update, and fix [#1013191](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1013191). No upload needed yet.
* monty: New usptream release 2022.4.26. Start building documentation instead of using the pre build documentation. Waiting for sponsorship.
* jupyter-packaging: New upstream release 0.12.2. Uploaded to unstable.
* python-scp: New upstream release 0.14.4. Waiting for sponsorship.
* unmask: New upstream release 2.0.0. Waiting for [sponsorship](https://salsa.debian.org/rust-team/debcargo-conf/-/merge_requests/348)

# Other projects

## CPython
* Clarify buffering args in open() function. https://github.com/python/cpython/pull/93685
