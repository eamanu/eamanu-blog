---
title: My contributions to open source - July 2022
date: 31 July, 2022
author: eamanu
tags: new-blog, Debian, python, open-source
---
# My contribution to Open Source - July 2022

## Debian
This is the list of the contributions:

* python-flask-jwt-extended: new usptream release 4.4.2. Uploaded to [unstable](https://incoming.debian.org/debian-buildd/pool/main/p/python-flask-jwt-extended/python-flask-jwt-extended_4.4.2-1.dsc).
* python-xlib: new upstream release. Waiting for sponsorhip.
* monty: new upstream release. Waiting for sponsorship.
* rust-umask: New upstream release 2.0.0. Uploaded by [Sylvestre Ledru](https://tracker.debian.org/news/1347147/accepted-rust-umask-200-1-amd64-source-into-unstable/)
* poetry-core: New upstream release 1.0.8. Uploaded by [Louis-Philippe Véronneau](https://tracker.debian.org/news/1346712/accepted-poetry-core-108-1-source-into-unstable/)
* poetry-core: Upload 1.1.0-b3 to experimental. In progress.
* markdown-it-py: Fix [#1013204](https://bugs.debian.org/1013204) whishlist bug. Patch from David Paul was applied.
* python-marshmallow-sqlalchemy: Update version to 0.28.1. Waiting for sponsorship.

# Other projects
Nothing this month :(

