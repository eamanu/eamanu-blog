---
title: My contributions to open source from someday - November 2021
date: Dec 05, 2021
author: eamanu
tags: new-blog, Debian, python, open-source
---

# My contribution to Open Source from Someday in the past to November

This is my first entry since I remove my hosting with WordPress. So, now I'm using
ablog <https://ablog.readthedocs.io>, and I move my code (include this blog) from
GitHub to Gitlab after the well know GitHub Copilot <https://copilot.github.com>.

This months I still working in FOSS (of course), and I have a list that I remember
that I worked, next month I hope have a more complete and detailed list of contributions.

## Debian

This is the list for Debian project:

  *  Poetry packaging: Yes, finally that work is finished <https://lists.debian.org/debian-python/2021/11/msg00025.html> thanks to everybody that help me, specially morph that help me a lot of to finish. This work include several package that I need to introduce to Debian:
      +  Poetry-core
      +  cleo
      +  clikit
      +  pastel
      + pylev
      + crashtest
      +  shellingham
  * Close RC bug for flask-openid <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=997635>
  * Working in the mod-wsgi RC bug <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=999857>
  * Fix of <https://bugs.debian.org/997308> and upload of python3-flask-jwt-extended.
  * Intent to package of myst_parser (in NEW for now) <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=993553> for this we need:
      + mdit_py_plugins, already in NEW <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=998906>
  * Fix of <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=997465> and <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=989270> for python3-marshmallow-sqlalchemy package.
  * etc...


## Other projects

This is the list for other contributions/work not in Debian:

  * Pytest: Fix <https://github.com/pytest-dev/pytest/issues/8994> via <https://github.com/pytest-dev/pytest/pull/9066>
  * Pytest: Fix <https://github.com/pytest-dev/pytest/issues/8947> via <https://github.com/pytest-dev/pytest/pull/8962>
  * Implementation of the Digital Clinical History in my city (perhaps more detail in another entry)
  * Working in several implementations of free software an open source in my city.


This is all for now. :-)



