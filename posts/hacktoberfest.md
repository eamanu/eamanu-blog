---
author: eamanu
blogspot: true
date: Oct 16, 2018
tags: Old-Blog
title: Hacktoberfest

---
# Hacktoberfest

Hi! [Hacktoberfest](https://hacktoberfest.digitalocean.com/) is my first hackaton in my life. This is the fifth hackaton organized by digital ocean. The challenge is made 5 PR and you will win a T-Shirt. This year there are 50,000 T-shirt. So, good luck for me :-).

Up to date 2018-10-16:

According to the [stats page](https://hacktoberfest.digitalocean.com/stats/eamanu) I contribute with 6/5 repository:

- sunpy/sunpy
- socialcopsdev/camelot
- ManuelGil/Hacktoberfest
- scikit-learn/scikit-learn

Update 2018-10-30:

[Hacktoberfest](https://hacktoberfest.digitalocean.com/) was finish! And I can complete the challenge. This was my first hackhaton in my life and it was great! It help me to know a lot of open source project that are very interesting.

In the last week Digital Ocean send my a mail to let me know that I complete the challenge and that I win a T-Shirt. So, now is time to wait for it (more or less I have to wait a month). I wish that my next hackhaton be in person.

I finished the Hackhaton with the next contributions:

- 2 Contributions to [sunpy/sunpy](http://github.com/sunpy/sunpy)
- 2 Contributions to [jooaodanieel/GCommit](http://github.com/jooaodanieel/GCommit)
- 1 Contribution to [socialcopsdev/camelot](http://github.com/socialcopsdev/camelot)
- 2 Contributions to [ManuelGil/Hacktoberfest](http://github.com/ManuelGil/Hacktoberfest)
- 2 Contributions to [scikit-learn/scikit-learn](http://github.com/scikit-learn/scikit-learn)

Cheers!