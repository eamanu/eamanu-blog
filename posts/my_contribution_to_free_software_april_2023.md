---
title: My contributions to Free Software - April 2023
date: 30 April, 2023
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - April 2023

## Debian
This is the list of the contributions:

* python-fsspec: New upstream release 2023.3.0 to be uploaded to experimental. Waiting for sponsorship.
* cookiecutter: Apply an upstream patch to fix [#1033431](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1033431). Uploaded to unstable by [Vincent Bernat](https://tracker.debian.org/news/1428317/accepted-cookiecutter-173-3-source-into-unstable/). Thanks.
* markdown-it-py: Apply upstream patches to fix CVE-2023-26302 and CVE-2023-26303 security issues, reported in [#1031764](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1031764). Uploaded to unstable by [Santiago Ruano Rincón](https://tracker.debian.org/news/1428692/accepted-markdown-it-py-210-5-source-into-unstable/). Thanks.
* mdit-py-plugin: New upstream release 0.3.5 for experimental. Waiting for sponsorship.
* python-scp: New upstream release 0.14.5 for experimental. Waiting for sponsorship.
* palettable: New upstream release 3.3.1 for experimental. Waiting for sponsorship.


# Other projects

Nothing for now.