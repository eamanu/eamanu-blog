---
author: eamanu
blogspot: true
date: Apr 17, 2017
tags: Old-Blog
title: Escribir mensajes de commit en Git

---
# Escribir mensajes de commit en Git

<div style="text-align: justify;">Llega un momento en la vida de un programador (no cualquier programador, sino aquel que persigue la filosfía de software libre), que quiere comenzar a contribuir en algún proyecto de software libre. Muchas veces el problema, es seleccionar un proyecto. Después de eso, nos damos cuenta que el código es demasiado complejo, el proyecto está muy avanzado, y nos sentimos que somos el más inútil de todos los programadores. Pero ese no es el motivo de esta entrada. Por lo pronto, un buen psicólogo, horas de lectura, y sobre todo perseverancia, serán suficientes para lograr alcanzar la meta (hacer un pull request y que sea aceptado).</div><div style="text-align: justify;"></div><div style="text-align: justify;">El motivo de esta entrada, es aprender un poco sobre la convención que existe sobre cómo escribir mensajes de commit en el mundo del software libre (si, sobre que ya es complejo meter un línea de código, los mensajes de commit también tienen reglas).</div><div style="text-align: justify;"></div><div style="text-align: justify;">Por ejemplo, esta es la [guía para escribir mensajes](https://git.kernel.org/pub/scm/git/git.git/tree/Documentation/SubmittingPatches?id=HEAD) de commit que utiliza el proyecto Git.</div><div style="text-align: justify;"></div><div style="text-align: justify;">Resumiendo las cosas más importantes, las recomendaciones son las siguientes:</div><div style="text-align: justify;"></div>- La primera línea de la descripción debería ser una descripción corta (aprox. 50 caractéres) y no se debe escribir el punto final.
- En el cuerpo del mensaje debe cuidarse de: 
  - usar verbos en presente, por ejemplo «cambio», vez de «cambié».
  - incluir las motivaciones del cambio, y contrastar su implementación con el comportamiento anterior.

<div><div style="text-align: justify;">Aquí dejo un ejemplo de mensaje de commit (en inglés):</div><div style="text-align: justify;"></div></div>```
<pre style="background-color: white; font-size: 15px; overflow: auto;">```
Capitalized, short (50 chars or less) summary

More detailed explanatory text, if necessary.  Wrap it to about 72
characters or so.  In some contexts, the first line is treated as the
subject of an email and the rest of the text as the body.  The blank
line separating the summary from the body is critical (unless you omit
the body entirely); tools like rebase can get confused if you run the
two together.

Write your commit message in the imperative: "Fix bug" and not "Fixed bug"
or "Fixes bug."  This convention matches up with commit messages generated
by commands like git merge and git revert.

Further paragraphs come after blank lines.

- Bullet points are okay, too

- Typically a hyphen or asterisk is used for the bullet, followed by a
  single space, with blank lines in between, but conventions vary here

- Use a hanging indent



```
```

Un buen mensaje de commit debería poder responder las siguientes preguntas:

- **¿Por qué esto es necesario?**
- **¿Cómo aborda el problema?**
- **¿Cuál es el efecto de este nuevo código?**

<div><div style="text-align: justify;">Si tenemos bien en mente esto y podemos reponderlas, el siguiente paso es plasmar estas respuestas en un mensaje de commit.</div></div><div></div><div><div style="text-align: justify;">En una de las fuentes que dejo más abajo, describe una serie de **cosas que no hay que hacer**, que pueden ser interesantes:</div></div><div>- No utilizar los sistemas de versionado como Backup. Existen muchos programadores (yo por ejemplo), que hemos usado o usamos, el versionado como una especie de backup. El resultado es poco útil, con una gran cantidad de diff aleatorios con cambios que son imposibles de entender y de seguir.
- Muchas veces los cambios que realizamos involucran varios archivos. El commit no debe realizarse por cada archivo, sino uno solo por problema.
- No utilizar mensajes tontos, sin sentidos, o con etiquetas.
- No se deben realizar varios cambios en un solo commit.
- No se debe dejar espacios en blanco, esto confunde.

<div><div style="text-align: justify;">Vuelvo a recalcar sería interesante que lean la guía para escribir mensajes de commit del mismísimo Git: <https://git.kernel.org/pub/scm/git/git.git/tree/Documentation/SubmittingPatches?id=HEAD></div></div></div><div></div><div><div style="text-align: justify;">**Hasta la próxima!**</div><div style="text-align: justify;"></div><div style="text-align: justify;">Fuentes:</div></div><div><div style="text-align: justify;"><http://365git.tumblr.com/post/3308646748/writing-git-commit-messages></div></div><div><div style="text-align: justify;"><http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html></div></div><div><div style="text-align: justify;"><http://who-t.blogspot.com.ar/2009/12/on-commit-messages.html></div></div>