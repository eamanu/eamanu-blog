---
title: My contributions to Free Software - January 2024
date: 31 January, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - January 2024

## Debian
* python-cassandra-drive: New upstream release 3.29.0. This version fix the build with Python3.12. Use nose instead of nose2 for tests. And add python3-pyasyncore becase upstream still use asyncore.
* gtts: New upstream release 2.5.1.

### Sponsored Package
* python-pandas-flavor: Introduce version 0.6.0 to Debian. Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.
* python-pyaarlo:  Introduce version 0.8.0.2 to Debian. Kudos to Carles Pina i Estany <carles@pina.cat>.
* python-ring-doorbell: Introduce 0.8.5 to Debian. Kudos to Carles Pina i Estany <carles@pina.cat>.
* circuit: Upload version 3.2.2-2. This version fix autopkgtest for python3.12, please see [#1059740](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1059657). Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.
* python-mongoengine: New upstream release 0.27.0. Kudos to Ananthu C V <weepingclown@disroot.org>.
* python-graphene-mongo: Introduce version 0.4.1 to Debian. Kudos to Ananthu C V <weepingclown@disroot.org>.
* python-disptrans: Remove support for numba until it migrate. Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.

## Other projects
* python-cassandra-driver: Replace deprecated class method assertRaisesRegexp for assertRaisesRegex. See [here](https://github.com/datastax/python-driver/pull/1195).
