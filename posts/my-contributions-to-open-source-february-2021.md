---
author: eamanu
blogspot: true
date: Mar 05, 2021
tags: Old-Blog
title: My contributions to Open Source; February 2021

---
# My contributions to Open Source; February 2021
-

<div class="wp-block-group"><div class="wp-block-group__inner-container">- Working on pylev 1.2.0-2 fixing [\#980725](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=980725). Upload to unstable. (Thanks Stefano Rivera for sponsorship)
- Working on python-cassandra-driver 3.24.0-1. Upload to unstable (Thanks Stefano Rivera for sponsorship).
- Working on shellinghma 1.3.2-1. Upload to unstable. (Thanks Sergio Durigan Junior for sponsorship)
- Working on poetry. Still in progress.
- Working on cachy 0.3.0-3. Upload to unstable. (Thanks for Stefano Rivera for sponsorship).
- Working on libcloud 3.2.0-2. Fixing [\#81769](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=981769). (Thanks Matthias Klose for patch and upload)
- Working on python-b2sdk 1.3.0-1. Fixing [\#975721](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=975721) and [\#981766](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=981766)
- Working on flask-jwt-simple. Improve autopkgtest. Uploaded to unstable.

</div></div>- Intent to adopt successful of cuyo. https://bugs.debian.org/916692
- Working on cocinelle 1.1.0. Uploaded to unstable.
- Fix citeproc-py repository on salsa and add tag.

**Open Source**







- Working on indep\_free (tool to send me newspaper) https://github.com/eamanu/indep\_free
- Open https://github.com/vimalloc/flask-jwt-simple/issues/20 and fixing here -&gt; https://github.com/vimalloc/flask-jwt-simple/pull/21
- Minor improve on documentation on CPython https://github.com/python/cpython/pull/24522
- Som reviews of PR on **python-docs-es**
