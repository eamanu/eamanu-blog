---
author: eamanu
blogspot: true
date: Jan 24, 2021
tags: Old-Blog
title: "My contributions to Open Source \u2013 December 2020"

---
# My contributions to Open Source – December 2020

*Better late than never*

Debian




- Some works on python-blosc, now is building successfully in testing thanks of Graham Inggs.
- Work on a new upstream release of python-molotov. But that need aiodogstatsd.
- Package of aiodogstatsd. Uploaded to unstable (Sponsored by Matthias Klose)
- Start working hard to package poetry on Debian so:
  - packaging of poetry-core. (Sponsored by Louis-Philippe Véronneau)
  - Work on python-cleo (ITP bug: #958366)
  - Packaging of python-pastel (sponsored by Jonathan Carter) https://tracker.debian.org/pkg/pastel
  - Packaging of python-pylev (sponsored by Jonathan Carter) https://tracker.debian.org/pkg/pylev
  - Packaging of crashtest in NEW (sponsored by Jonathan Carter) https://ftp-master.debian.org/new/crashtest\_0.3.1-1.html
- Fix #975844 on sphinx-tab 1.2.1+ds-3

Open source



--

- 2 open issues on WiFi-EvilTwin:
  - https://github.com/ShlomiRex/WiFi-EvilTwin/issues/3
  - https://github.com/ShlomiRex/WiFi-EvilTwin/issues/2
- 2 open PR to fix some issues on WiFi-EvilTwin:
  - https://github.com/ShlomiRex/WiFi-EvilTwin/pull/5
  - https://github.com/ShlomiRex/WiFi-EvilTwin/pull/4
- Some issues open on https://github.com/pkcarlislellc/papershaper that upstream want to package its project in Debian.
- Try to python-molotov upstream start release signature https://github.com/loads/molotov/issues/129
- Reviews PR in some projects: python-docs-es, learn-regex and cpython.
- PR open to fix a possible issue on documentation on cpython https://github.com/python/cpython/pull/23660 mark as stale 🙁
- Fix minor issues on python-docs-es project:
  - https://github.com/python/python-docs-es/pull/1170
  - https://github.com/python/python-docs-es/pull/1162
  - use of pathlib module on conf.py https://github.com/python/python-docs-es/pull/1171
- An educational project used on the Python Intermediate course that I give on The National University of La Rioja (Argentina) https://github.com/eamanu/treefiles. This is a WIP project to mimic tree linux command.
- Start Unlarnianos projects, a forum for The National University of La Rioja (Argentina) based on FlaskBB https://github.com/eamanu/unlarnianos
- Develop of pydebdep, to know the whole python dependency of a project that are not in Debian yet. Used to calculate the calculate work effort to pacakge Poetry on Debian. https://github.com/eamanu/pydebdep beta version.