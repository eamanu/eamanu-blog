---
title: My contributions to Free Software - March 2024
date: 31 March, 2024
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - March 2024

## Debian
* monty: New upstream release monty_2024.2.26.
* scikit-build-core: New upstream release 0.8.2.
* scalene: New upstream release 1.5.37.
* basemap: Fix basemap doc package. Check the [commit](https://salsa.debian.org/python-team/packages/basemap/-/commit/c5c76640dc823cdee819ee29b2199946a387397b) for more details.
* i3pystatus: Upload 3.35+git20191126.5a8eaf4-2.1 that fix RC [#1056410](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1056410)
* python-clycer: Adopt [orphaned package](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065250). New upstream release 0.12.1. Closes [#1046161](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1046161) and [#1049518](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1049518).
* python-dmidecode: Adopt [orphaned package](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065233).
* python-hpilo: Adopt [orphaed package](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=888079).
* python-cryptography-vectors: New upstream release 42.0.5. Also, adopt [orphaned package](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1064977).
* ipykernel: New upstream release 6.29.3. Closes the next RC bugs [#1056413](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1056413) and [#1058305](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1058305)
* contourpy: Adopt [orphaned package](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065246)
* kiwisolver: Adopt [orphaned package](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1065244). Fix bug [#1063464](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1063464).

### Sponsored Package
* python-googleapis-common-protos: Introduce version 1.62.0 to Debian. Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.
* python-chocolate: Introduce version 0.0.2 to Debian. Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.
* simplemonitor: New upstream release 1.12.1. Kudos to carles Pina i Estany <carles@pina.cat>
* python-rpcq: Introduce version 3.11.0 to Debian. Kudos to Yogeswaran Umasankar <kd8mbd@gmail.com>.
* python-ring-doorbell: New upstream version 0.8.7. Kudos to carles Pina i Estany <carles@pina.cat>.
* python-mongoengine: New upstream release 0.28.2. Kudos to Ananthu C V <weepingclown@disroot.org>.
* python-graphene-mongo: New upstream release _0.4.3. Kudos to Ananthu C V <weepingclown@disroot.org>.
* importlab: New upstream release 0.8.1. Go to experimental. Kudos to Lev Borodin <faunris@gmail.com>.
* python-sphinx-examples: Introduce version 0.0.5. Kudos to Ananthu C V <weepingclown@disroot.org>.
* controku: Introduce version 1.1.0. Kudos to Ben Westover <me@benthetechguy.net>.

## Other projects

