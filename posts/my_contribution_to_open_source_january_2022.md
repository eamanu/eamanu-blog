---
title: My contributions to open source - January 2022
date: Jan 31, 2022
author: eamanu
tags: new-blog, Debian, python, open-source
---
# My contribution to Open Source - January 2022

## Debian
This is the list of the contributions:

* flask-jwt-simple: fix <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1002323>. Uploaded.
* Cuyo: some updates in the package. Fix <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=766715>. Request for sponsorship and waiting for that.
* fsspec: New upstream release 021.11.1 Closes: <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=983883>. Several improves in the package. Still in progress.
* shellingham: New upstream release 1.4.0. Closes: <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1001485>. Uploaded by morph.
* mdit-py-plugins: New upstream release. Uploaded by morph.
* myst-parser: New upstream release. Fix failed tests in Debian CI. Uploaded by morph.
* pythonmagick: Fix RC <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=999781>. Uploaded by Jeroen Ploemen.
* python-cleo: Made package reproducible (adding LANGUAGE=en env when the docs is building). Start building package using poetry. Some updates. Uploaded.
* python-ftputil: New upstream release and some improves in the package. Waiting for sponsorship.
* aiodogstatsd: New upstream release and several improves in the package. Waiting for sponsorship.

## Pytest
* Minor improve in doc <https://github.com/pytest-dev/pytest/pull/9506>. Clarify the location of `configuration` file.
