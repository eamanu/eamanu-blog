---
title: April 2022
date: 30 Apr, 2022
author: eamanu
tags: new-blog, Debian, python, open-source
---
# My contribution to Open Source - April 2022

## Debian
This is the list of the contributions:

* mdurl, uploaded to NEW by [Jeroen Ploemen](https://ftp-master.debian.org/new/mdurl_0.1.0-1.html). Accepted in Debian https://tracker.debian.org/pkg/mdurl.
  - New upstream version v0.1.1. Uploaded by [Jeroen Ploemen](https://tracker.debian.org/news/1319992/accepted-mdurl-011-1-source-into-unstable/)
* fsspec: New upstream realease v2022.3.0. Uploaded to unstable by [Jeroen Ploemen](https://tracker.debian.org/news/1317743/accepted-fsspec-202230-1-source-into-unstable/). Builds is failing.
* jupyter-packaging, New usptream release v0.12.0. Activate tests and autopkgtest again. Uploaded to unstable https://tracker.debian.org/news/1317971/accepted-jupyter-packaging-0120-1-source-all-into-unstable/
* markdown-it-py, new upstream release v2.0.1. This verison start using mdurl already introduced in Debian. Uploaded by [bage@debian.org](https://tracker.debian.org/pkg/markdown-it-py)
* jcc: new upstream release 3.11. Waiting for sponsorship.
* monty: working in new upstream release 2022.4.26.

