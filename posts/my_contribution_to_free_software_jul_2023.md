---
title: My contributions to Free Software - July 2023
date: 31 July, 2023
author: eamanu
tags: new-blog, Debian, python, free-software
---
# My contribution to Free Software - July 2023

## Debian
This is the list of the contributions:

* poetry-core: Upload new upstream version 1.6.1 to [experimental](https://tracker.debian.org/news/1442465/accepted-poetry-core-161-1exp1-source-into-experimental/) 
* monty: build in some archs. monty now depends on python3-torch, this package is not build for all architecture, monty should follow this approach. Uploaded to [unstable](https://tracker.debian.org/news/1444641/accepted-monty-202358dfsg-4-source-into-unstable/)
* fsspec: migrate 2023.6.0 version from testing to [unstable](https://tracker.debian.org/news/1440532/accepted-fsspec-202360-1-source-into-unstable/).

I'm happy to say: I am a Debian Developer now :-). See: https://nm.debian.org/person/eamanu/

# Other projects
* Fix of blurb link in python devguide. See [PR](https://github.com/python/devguide/pull/1137). See [issue](https://github.com/python/devguide/issues/1136).

