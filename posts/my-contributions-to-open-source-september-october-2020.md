---
author: eamanu
blogspot: true
date: Nov 08, 2020
tags: Old-Blog
title: "My contributions to Open Source \u2013 September/October 2020"

---
# My contributions to Open Source – September/October 2020

I cut my finger’s tendon with a knife, so my contributions was down.

On september

- Fix typo: https://github.com/grisha/mod\_python/pull/94
- Minor change on cpython documentation: https://github.com/python/cpython/commit/94bfdee25db31941b187591ae5ae9bf3ed431090
- Minor fix on cpython documentation https://github.com/python/cpython/pull/22407
- Upload flask-jwt-simple to untable. (Debian)
- Upload flask-jwt-extendend to unstable. (Debian)
- Ready for upload flask-openid. (Debian)
- Upload to NEW by Andrius Merkys (thanks!) of palettable. (Debian)
- Upload to NEW of monty. (Debian)
- Prepare for release libapache2-mod-python (Debian)
- Working on python-xlib (debian)
- Prepare for upload to NEW umask (rust package) (Debian)
- Working on libcloud (Debian)
- Working on flask-restful (Debian)

On october

- Open a issue on blist for python3.9 ftbs https://github.com/DanielStutzbach/blist/issues/90.
- Open an issue on pyvows for python3.9 ftbs https://github.com/heynemann/pyvows/issues/136
- monty on Debian 🙂
- Upload flask-openid to unstable by Jonathan Carter (thanks!)
- Upload python-potr to unstable by Jonathan Carter (thanks!)
- Upload fsspec to unstable by Matthias Klose (thanks!)
- Working on s2geometry
- Upload to NEW and unstable by Matthias Klose (Thanks!)

Also during the hacktoberfest I was help on python-docs-es reviewing PRs and I prepared a talk for PyConAr2020.

Also, I reviewed some CPython PRs and working on PIP projects. I hope be more active on that projects.